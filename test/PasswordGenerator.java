import efi.unleashed.security.DigestAuthenticationAwarePasswordEncoder;

/**
 * This class is used to generate the hex value for an encrypted password for manual insertion in the database.
 * See the encoder source and the Spring security setup if values change.
 */
public class PasswordGenerator {

    public static void main( String[] args ) {
        if ( args.length != 2 ) {
            System.err.println( "Usage: PasswordGenerator <username> <password>" );
            System.exit( 2 );
        }
        String username = args[0];
        String password = args[1];
        String realm = "ReServe Unleashed Digest Security Realm";
        String toEncode = String.format( "%s:%s:%s", username, realm, password );

        DigestAuthenticationAwarePasswordEncoder encoder = new DigestAuthenticationAwarePasswordEncoder();
        System.out.println( encoder.md5Hex( toEncode ) );
        System.exit( 0 );
    }
}
