package efi.platform.mocks;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class is used to intercept outbound email messages, so tests don't need to rely on external an external SMTP
 * service.  It also keeps track of send emails, so tests can verify mail is sent.
 */
public class MockMailSender extends JavaMailSenderImpl {

    private static List<SimpleMailMessage> sentEmails = new ArrayList<SimpleMailMessage>();

    public static List<SimpleMailMessage> getSentEmails() {

        return sentEmails;
    }

    public static void reset() {

        sentEmails.clear();
    }

    @Override
    public void send( SimpleMailMessage simpleMessage ) throws MailException {

        sentEmails.add( simpleMessage );
    }

    @Override
    public void send( SimpleMailMessage[] simpleMessages ) throws MailException {

        if ( simpleMessages != null ) {
            sentEmails.addAll( Arrays.asList( simpleMessages ) );
        }
    }
}
