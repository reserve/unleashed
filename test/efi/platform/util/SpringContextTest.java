package efi.platform.util;


import efi.platform.domain.ApplicationInformation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class SpringContextTest {

    // Make sure @Configuration works correctly.
    @Test
    public void testSpringStartup() throws Exception {
        ApplicationInformation info =
                (ApplicationInformation) SpringApplicationContext.getBean( "applicationInformation" );
        assertNotNull( info );
    }
}
