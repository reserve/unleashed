package efi.platform;

import efi.platform.service.ErrorMessageService;

import org.springframework.context.MessageSourceResolvable;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.List;

/**
 * This class provides common helper methods for unit testing.
 */
public class TestingUtils {

    private TestingUtils() {} // only static access

    public static List<String> getDisplayableErrors( Errors errors, ErrorMessageService service ) {

        return errors.hasErrors() ? service.getDisplayableErrors( errors ) : new ArrayList<String>();
    }

    public static boolean errorsContains( Errors errors, ErrorMessageService service, String messageFragment ) {

        if ( !errors.hasErrors() ) {
            return false;
        }
        List<String> messages = service.getDisplayableErrors( errors );
        return errorsContains( messages, messageFragment );
    }

    public static boolean errorsContains( List<String> messages, String messageFragment ) {

        if ( messages == null || messages.isEmpty() ) {
            return false;
        }
        boolean foundMessage = false;
        for ( String message : messages ) {
            if ( message.contains( messageFragment ) ) {
                foundMessage = true;
                break;
            }
        }
        return foundMessage;
    }

    public static String getDisplayableMessage( MessageSourceResolvable resolvable, ErrorMessageService service ) {

        return service.getDisplayableError( resolvable );
    }

    public static boolean messageContains( MessageSourceResolvable resolvable,
                                         ErrorMessageService service,
                                         String messageFragment ) {

        String message = service.getDisplayableError( resolvable );
        return messageContains( message, messageFragment );
    }

    public static boolean messageContains( String message, String messageFragment ) {

        if ( message == null || message.length() == 0 ) {
            return false;
        }

        return message.contains( messageFragment );
    }
}
