package efi.unleashed.security;

import efi.unleashed.domain.RightType;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;

import java.util.ArrayList;
import java.util.List;

/**
 * This class interactions with the Spring Security subsystem for testing purposes.
 * It returns an authentication with all available rights, to bypass the @Secure annotations.
 */
public class TestSecurityContext implements SecurityContext {

    @Override
    public Authentication getAuthentication() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        for ( RightType rightType : RightType.values() ) {
            grantedAuthorities.add( new SimpleGrantedAuthority( rightType.name() ) );
        }
        return new UsernamePasswordAuthenticationToken( "", "", grantedAuthorities );
    }

    @Override
    public void setAuthentication( Authentication authentication ) {
        // Unused
    }
}
