package efi.unleashed.dao.impl;

import efi.unleashed.dao.EnterpriseDao;
import efi.unleashed.domain.Enterprise;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class EnterpriseDaoTest {

    @Resource
    EnterpriseDao enterpriseDao;

    @Test
    public void testFindByShortName() throws Exception {

        final String SHORT_NAME = "Frontier";
        final String NAME = "Frontier Vineyards";
        Enterprise enterprise = enterpriseDao.findByShortName( SHORT_NAME );
        assertNotNull( enterprise );
        assertEquals( NAME, enterprise.getName() );
    }
}
