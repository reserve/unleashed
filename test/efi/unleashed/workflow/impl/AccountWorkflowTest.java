package efi.unleashed.workflow.impl;

import efi.platform.TestingUtils;
import efi.platform.service.ErrorMessageService;
import efi.platform.web.controller.WorkflowErrors;
import efi.unleashed.TestConstants;
import efi.unleashed.domain.Account;
import efi.unleashed.domain.Address;
import efi.unleashed.security.SessionUserContext;
import efi.unleashed.service.AccountService;
import efi.unleashed.service.EnterpriseService;
import efi.unleashed.view.web.AccountView;
import efi.unleashed.view.web.AddressView;
import efi.unleashed.workflow.AccountWorkflow;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class AccountWorkflowTest {

    @Resource
    AccountWorkflow accountWorkflow;

    @Resource
    AccountService accountService;

    @Resource
    ErrorMessageService errorMessageService;

    @Resource( name = "scopedTarget.sessionUserContext" )
    SessionUserContext userContext;

    @Resource
    EnterpriseService enterpriseService;

    @Before
    public void setupUserContext() {

        userContext.setUserId( TestConstants.TEST_USER_ID );
        userContext.setEnterpriseId( TestConstants.TEST_ENTERPRISE_ID );
    }

    @Test
    public void testFindContact() throws Exception {

        WorkflowErrors errors = new WorkflowErrors();
        AccountView accountView = accountWorkflow.findAccount( TestConstants.TEST_ACCOUNT_ID_NO_CONTACTS, errors );
        assertNotNull( accountView );
        assertEquals( accountView.getName(), "ReServe Interactive Wisconsin Office" );
    }

    @Test
    public void testInvalidNullFindContact() throws Exception {

        //First test if accountId is null
        WorkflowErrors errors = new WorkflowErrors();
        AccountView accountViewNull = accountWorkflow.findAccount( null, errors );

        List<String> messages = TestingUtils.getDisplayableErrors( errors, errorMessageService );
        assertTrue( messages.size() == 1 );
        int messageNullMatchCount = 0;
        for ( String message : messages ) {
            if ( message.contains( "Please select an account." ) ) {
                messageNullMatchCount++;
            }
        }
        assertTrue( messageNullMatchCount == 1 );

        //Now check if accountId is invalid
        errors = new WorkflowErrors();
        AccountView accountViewInvalid = accountWorkflow.findAccount( TestConstants.TEST_UNUSED_ID, errors );
        messages = TestingUtils.getDisplayableErrors( errors, errorMessageService );
        assertTrue( messages.size() == 1 );
        int messageInvalidIdCount = 0;
        for ( String message : messages ) {
            if ( message.contains( "The selected account could not be found." ) ) {
                messageInvalidIdCount++;
            }
        }
        assertTrue( messageInvalidIdCount == 1 );
    }

    @Test
    public void testInvalidNullAddAccount() throws Exception {
        //This test covers both the null object condition
        //and specific field validation
        WorkflowErrors errors = new WorkflowErrors();
        AccountView accountView = new AccountView();
        AccountView response = accountWorkflow.addAccount( accountView, errors );
        assertNull( response );

        List<String> messages = errorMessageService.getDisplayableErrors( errors );
        assertEquals( 1, messages.size() );
        Integer messageContainsMayNotBeEmpty = 0;
        Integer messageContainsMayNotBeNull = 0;
        for ( String message : messages ) {
            if ( message.contains( "may not be empty" ) ) {
                messageContainsMayNotBeEmpty++;
            }
        }
        assertTrue( messageContainsMayNotBeEmpty.equals( 1 ) );
    }

    @Test
    public void testAddAccount() throws Exception {

        AccountView accountView = new AccountView();
        final String acccountNumber = "A34633";
        final String name = "Roger Rabbit's Carrot Cafe";
        final String webSite = "http://rogerrabbit.com";
        final String description = "Tons of delicious carrots and radishes";
        final String phone = "6544444444";
        final String fax = "4733332222";

        String address1 = "12332 Test way";
        String address2 = "Suite A";
        String city = "Pleasanton";
        String state = "CA";
        String zipCode = "94551";
        String country = "United States";

        Address address = new Address();
        address.setAddress1( address1 );
        address.setAddress2( address2 );
        address.setCity( city );
        address.setState( state );
        address.setZipCode( zipCode );
        address.setCountry( country );

        accountView.setAccountNumber( acccountNumber );
        accountView.setName( name );
        accountView.setWebSite( webSite );
        accountView.setDescription( description );
        accountView.setPhone( phone );
        accountView.setFax( fax );
        accountView.setAddressView( new AddressView( address ) );
        WorkflowErrors errors = new WorkflowErrors();
        AccountView response = accountWorkflow.addAccount( accountView, errors );

        assertNotNull( response );

        Account accountFromDb = accountService.findAccountForEnterprise( response.getId(),
                                                                         TestConstants.TEST_ENTERPRISE_ID );
        assertNotNull( accountFromDb );
        assertEquals( acccountNumber, accountFromDb.getAccountNumber() );
        assertEquals( name, accountFromDb.getName() );
        assertEquals( webSite, accountFromDb.getWebSite() );
        assertEquals( description, accountFromDb.getDescription() );
        assertEquals( phone, accountFromDb.getPhone() );
        assertEquals( fax, accountFromDb.getFax() );
        assertEquals( accountFromDb.getAddress().getAddress1(), address1 );
        assertEquals( accountFromDb.getAddress().getAddress2(), address2 );
        assertEquals( accountFromDb.getAddress().getCity(), city );
        assertEquals( accountFromDb.getAddress().getState(), state );
        assertEquals( accountFromDb.getAddress().getZipCode(), zipCode );
        assertEquals( accountFromDb.getAddress().getCountry(), country );
    }

    @Test
    public void testUpdateAccount() throws Exception {

        WorkflowErrors errors = new WorkflowErrors();
        AccountView accountView = accountWorkflow.findAccount( TestConstants.TEST_ACCOUNT_ID_THREE_CONTACTS, errors );
        //Change values
        accountView.setAccountNumber( "UPDACCT#" );
        accountView.setName( "Updated Company Name" );
        accountView.setWebSite( "http://updatedwebsite.com" );
        accountView.setDescription( "Updated Desc." );
        accountView.setPhone( "1111111111" );
        accountView.setFax( "7676767676" );

        Address address = new Address();
        address.setAddress1( "Addy1" );
        address.setAddress2( "Addy2" );
        address.setCity( "CityUpdated" );
        address.setState( "StateUpdated" );
        address.setZipCode( "ZipUpdated" );
        address.setCountry( "CountryUpdated" );
        accountView.setAddressView( new AddressView( address ) );
        errors = new WorkflowErrors();
        accountWorkflow.updateAccount( accountView, errors );

        AccountView lookedUpAccountView = accountWorkflow.findAccount( accountView.getId(), errors );
        assertEquals( lookedUpAccountView.getAccountNumber(), accountView.getAccountNumber() );
        assertEquals( lookedUpAccountView.getName(), accountView.getName() );
        assertEquals( lookedUpAccountView.getWebSite(), accountView.getWebSite() );
        assertEquals( lookedUpAccountView.getDescription(), accountView.getDescription() );
        assertEquals( lookedUpAccountView.getPhone(), accountView.getPhone() );
        assertEquals( lookedUpAccountView.getFax(), accountView.getFax() );
        assertNotNull( lookedUpAccountView.getAddressView() );
        assertTrue( lookedUpAccountView.getAddressView()
                            .getAddress1()
                            .equals( accountView.getAddressView().getAddress1() ) );
    }

    @Test
    public void testInvalidNullUpdateAccount() throws Exception {

        WorkflowErrors errors = new WorkflowErrors();
        AccountView accountView = new AccountView();
        accountWorkflow.updateAccount( accountView, errors );
        List<String> messages = TestingUtils.getDisplayableErrors( errors, errorMessageService );
        assertTrue( messages.size() == 1 );
        int mayNotBeEmptyCount = 0;
        for ( String message : messages ) {
            if ( message.contains( "may not be empty" ) ) {
                mayNotBeEmptyCount++;
            }
        }
        assertTrue( mayNotBeEmptyCount == 1 );
    }

    @Test
    public void testInvalidBadDataUpdateAccount() throws Exception {

        WorkflowErrors errors = new WorkflowErrors();
        AccountView accountView = accountWorkflow.findAccount( TestConstants.TEST_ACCOUNT_ID_NO_CONTACTS, errors );
        //Change values
        accountView.setId( TestConstants.TEST_UNUSED_ID );
        accountView.setName( "Company Name" );

        errors = new WorkflowErrors();
        accountWorkflow.updateAccount( accountView, errors );

        List<String> messages = TestingUtils.getDisplayableErrors( errors, errorMessageService );
        assertTrue( messages.size() == 1 );
        int messageMatchCount = 0;
        for ( String message : messages ) {
            if ( message.contains( "The selected account could not be found." ) ) {
                messageMatchCount++;
            }
        }
        assertTrue( messageMatchCount == 1 );
    }
}
