package efi.unleashed.domain;

import efi.unleashed.dao.EnterpriseDao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class SequenceTest {

    @Resource
    EnterpriseDao enterpriseDao;
    
    @Test
    public void testIdFromSequence() throws Exception {
        Enterprise enterprise = Enterprise.newInstance();
        enterprise.setName( "New Enterprise" );
        enterprise.setShortName( "New" );
        enterpriseDao.saveEntity( enterprise );
        enterpriseDao.refreshEntity( enterprise );
        assertTrue( enterprise.getId() >= 50000L );
    }
}
