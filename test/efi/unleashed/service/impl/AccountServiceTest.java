package efi.unleashed.service.impl;

import efi.unleashed.TestConstants;
import efi.unleashed.domain.Account;
import efi.unleashed.security.SessionUserContext;
import efi.unleashed.service.AccountService;
import efi.unleashed.service.EnterpriseService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class AccountServiceTest {

    @Resource( name = "scopedTarget.sessionUserContext" )
    SessionUserContext userContext;

    @Resource
    AccountService accountService;

    @Resource
    EnterpriseService enterpriseService;

    @Before
    public void setupUserContext() {

        userContext.setUserId( TestConstants.TEST_USER_ID );
        userContext.setEnterpriseId( TestConstants.TEST_ENTERPRISE_ID );
    }

    @Test
    public void testFindAccountForEnterprise() throws Exception {
        //Account with ID 100 has a name of "ReServe Interactive Wisconsin Office"
        Account account = accountService.findAccountForEnterprise( TestConstants.TEST_ACCOUNT_ID_NO_CONTACTS,
                                                                   userContext.getEnterpriseId() );

        assertNotNull( account );
        assertEquals( account.getName(), "ReServe Interactive Wisconsin Office" );
    }

    @Test
    public void testFindAccountsAndContactsForEnterprise() throws Exception {

        Account account = accountService.findAccountForEnterprise( TestConstants.TEST_ACCOUNT_ID_THREE_CONTACTS,
                                                                   userContext.getEnterpriseId() );

        assertNotNull( account );
        assertEquals( account.getContacts().size(), 3 );

    }

}
