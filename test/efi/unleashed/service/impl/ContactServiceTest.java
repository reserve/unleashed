package efi.unleashed.service.impl;


import efi.unleashed.TestConstants;
import efi.unleashed.domain.Contact;
import efi.unleashed.security.SessionUserContext;
import efi.unleashed.service.ContactService;
import efi.unleashed.service.EnterpriseService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class ContactServiceTest {

    @Resource( name = "scopedTarget.sessionUserContext" )
    SessionUserContext userContext;

    @Resource
    ContactService contactService;

    @Resource
    EnterpriseService enterpriseService;

    @Before
    public void setupUserContext() {

        userContext.setUserId( TestConstants.TEST_USER_ID );
        userContext.setEnterpriseId( TestConstants.TEST_ENTERPRISE_ID );
    }

    @Test
    public void testFindContactForEnterprise() throws Exception {

        //Contact with ID 100 has a first name of "Dan"
        Contact contact = contactService.findContactForEnterprise( userContext.getEnterpriseId(), 100L );

        assertNotNull( contact );
        assertEquals( contact.getFirstName(), "Dan");
    }


}
