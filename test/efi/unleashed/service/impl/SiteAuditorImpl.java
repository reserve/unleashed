package efi.unleashed.service.impl;

import efi.unleashed.dao.EnterpriseDao;
import efi.unleashed.domain.Site;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * This class is used to cause Envers auditing to happen in a separate transaction for the AuditServiceTest
 */
@Service
public class SiteAuditorImpl {

    @Resource
    EnterpriseDao enterpriseDao;

    @Transactional( readOnly = false )
    public Site updateSite( Site site ) {

        enterpriseDao.saveEntity( site );
        return enterpriseDao.getEntity( Site.class, site.getId() );
    }

}
