package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.unleashed.TestConstants;
import efi.unleashed.domain.Enterprise;
import efi.unleashed.security.SessionUserContext;
import efi.unleashed.service.EnterpriseService;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class EnterpriseServiceTest {

    @Resource( name = "scopedTarget.sessionUserContext" )
    SessionUserContext userContext;

    @Resource
    EnterpriseService enterpriseService;

    @Before
    public void setupUserContext() {
        userContext.setUserId( TestConstants.TEST_USER_ID );
        userContext.setEnterpriseId( TestConstants.TEST_ENTERPRISE_ID );
    }

    @Test
    public void testForCompleteness() throws Exception {
        assertNotNull( enterpriseService.getDefaultDao() );
    }

    @Test
    public void testFindAllEnterprises() throws Exception {
        List<Enterprise> enterprises = enterpriseService.loadAllEntities( Enterprise.class );
        assertNotNull( enterprises );
        assertFalse( enterprises.isEmpty() );
    }

    @Test
    public void testUpdate() throws Exception {
        Enterprise enterprise = enterpriseService.loadAllEntities( Enterprise.class ).get( 0 );
        DateTime previousDateTime = enterprise.getLastUpdated();
        int previousVersion = enterprise.getVersion();
        enterprise.setName( "New name" );
        GenericDao dao = enterpriseService.getDefaultDao();
        dao.flushEntities();
        dao.evictEntity( enterprise );
        enterprise = enterpriseService.loadAllEntities( Enterprise.class ).get( 0 );
        assertTrue( enterprise.getLastUpdated().isAfter( previousDateTime ) );
        assertTrue( enterprise.getVersion() > previousVersion );
    }

    @Test
    public void testContext() throws Exception {

        final String NAME = "Frontier Vineyards";
        Enterprise enterprise = enterpriseService.getEntity( Enterprise.class, userContext.getEnterpriseId() );
        assertNotNull( enterprise );
        assertEquals( NAME, enterprise.getName() );
    }
}
