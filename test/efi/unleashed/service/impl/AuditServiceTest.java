package efi.unleashed.service.impl;

import efi.unleashed.TestConstants;
import efi.unleashed.domain.AuditedEntityWrapper;
import efi.unleashed.domain.Site;
import efi.unleashed.security.SessionUserContext;
import efi.unleashed.security.TestSecurityContext;
import efi.unleashed.service.AuditService;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.util.List;

import javax.annotation.Resource;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * This test is a little different than other tests, since it is testing the Envers auditing feature.
 */
@Ignore( "Run this test separately, since it needs to commit its changes." )
@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = false )
public class AuditServiceTest {

    @Resource( name = "scopedTarget.sessionUserContext" )
    SessionUserContext userContext;

    @Resource
    AuditService auditService;

    @Resource
    SiteAuditorImpl siteAuditor;

    @Before
    public void setupUserContext() {
        userContext.setUserId( TestConstants.TEST_USER_ID );
        userContext.setEnterpriseId( TestConstants.TEST_ENTERPRISE_ID );
        SecurityContextHolder.setContext( new TestSecurityContext() );
    }

    @Test
    public void testUpdatesAreAudited() throws Exception {

        Site site = updateSite();
        List<AuditedEntityWrapper<Site>> history = auditService.getAuditHistory( Site.class, site.getId() );
        assertEquals( 2, history.size() );
    }

    @Rollback( false )
    public Site updateSite() {

        Site site = auditService.getDefaultDao().getEntity( Site.class, TestConstants.TEST_SITE_ID );
        assertNotNull( site );
        site.setName( "Update 1" );
        site = siteAuditor.updateSite( site );
        site.setName( "Update 2" );
        site = siteAuditor.updateSite( site );
        return site;
    }
}
