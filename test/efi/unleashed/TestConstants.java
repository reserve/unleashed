package efi.unleashed;

/**
 * This class contains constants used in many of the unit tests.
 */
public class TestConstants {

    public static final Long TEST_USER_ID = 101L;
    public static final Long TEST_ENTERPRISE_ID = 100L;
    public static final Long TEST_SITE_ID = 100L;

    public static final Long TEST_ACCOUNT_ID_NO_CONTACTS = 100L;
    public static final Long TEST_ACCOUNT_ID_THREE_CONTACTS = 101L;
    public static final Long TEST_CONTACT_ID_NO_ACCOUNT = 100L;
    public static final Long TEST_CONTACT_ID_WITH_ACCOUNT = 101L;

    public static final Long TEST_UNUSED_ID = 500L;

    public static final String TEST_BAD_ENUM_VALUE = "BAD_BAD_TYPE";

    public static final String TEST_USERNAME = "asmith";
}
