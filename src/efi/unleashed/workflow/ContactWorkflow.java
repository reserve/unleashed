package efi.unleashed.workflow;

import efi.unleashed.view.web.ContactAccountView;
import efi.unleashed.view.web.ContactView;

import org.springframework.validation.Errors;

import javax.validation.Valid;

public interface ContactWorkflow {

    public ContactView findContact( Long id, Errors errors );

    //TODO add find all contacts

    @Valid
    public ContactView addContact( @Valid ContactView contactView, Errors errors );

    @Valid
    public ContactView addAccountToContact( @Valid ContactAccountView contactView, Errors errors );

    @Valid
    public void updateContact( @Valid ContactView contactView, Errors errors );
}
