package efi.unleashed.workflow.impl;

import efi.unleashed.domain.Account;
import efi.unleashed.domain.Address;
import efi.unleashed.domain.Enterprise;
import efi.unleashed.security.SessionUserContext;
import efi.unleashed.service.AccountService;
import efi.unleashed.service.EnterpriseService;
import efi.unleashed.view.web.AccountView;
import efi.unleashed.workflow.AccountWorkflow;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

import javax.annotation.Resource;
import javax.validation.Valid;

@Service
public class AccountWorkFlowImpl implements AccountWorkflow {

    public static final Logger LOGGER = Logger.getLogger( AccountWorkFlowImpl.class.getName() );

    @Resource
    AccountService accountService;

    @Resource
    EnterpriseService enterpriseService;

    @Resource
    SessionUserContext sessionUserContext;

    @Override
    public AccountView findAccount( Long id, Errors errors ) {

        if ( id == null ) {
            errors.reject( "account.accountId.null" );
            return null;
        }

        Long enterpriseId = sessionUserContext.getEnterpriseId();
        if ( enterpriseId == null ) {
            errors.reject( "web.enterprise.not-found" );
        }

        Account account = accountService.findAccountForEnterprise( id, enterpriseId );
        if ( account == null ) {
            errors.reject( "account.notFound" );
        }

        if ( errors.hasErrors() ) {
            return null;
        }

        return new AccountView( account );
    }

    @Valid
    @Override
    @Transactional
    public AccountView addAccount( @Valid AccountView accountView, Errors errors ) {

        LOGGER.debug( "Attempting to add account to database: " + accountView );

        Long enterpriseId = sessionUserContext.getEnterpriseId();
        Enterprise enterprise = enterpriseService.findByEnterpriseId( enterpriseId );
        if ( enterprise == null ) {
            errors.reject( "web.enterprise.not-found" );
        }

        if ( errors.hasErrors() ) {
            return null;
        }

        Account account = new Account();
        account.setAccountNumber( accountView.getAccountNumber() );
        account.setName( accountView.getName() );
        account.setWebSite( accountView.getWebSite() );
        account.setDescription( accountView.getDescription() );
        account.setPhone( accountView.getPhone() );
        account.setFax( accountView.getFax() );
        account.setEnterprise( enterprise );

        if ( accountView.getAddressView() != null ) {
            Address address = new Address();
            address.setAddress1( accountView.getAddressView().getAddress1() );
            address.setAddress2( accountView.getAddressView().getAddress2() );
            address.setCity( accountView.getAddressView().getCity() );
            address.setState( accountView.getAddressView().getState() );
            address.setZipCode( accountView.getAddressView().getZipCode() );
            address.setCountry( accountView.getAddressView().getCountry() );
            account.setAddress( address );
        }

        accountService.getDefaultDao().saveEntity( account );
        return new AccountView( account );
    }

    @Valid
    @Override
    @Transactional
    public void updateAccount( @Valid AccountView accountView, Errors errors ) {

        LOGGER.debug( "Attempting to update account: " + accountView );

        Long enterpriseId = sessionUserContext.getEnterpriseId();
        Enterprise enterprise = enterpriseService.findByEnterpriseId( enterpriseId );
        if ( enterprise == null ) {
            errors.reject( "web.enterprise.not-found" );
        }

        Account account = accountService.findAccountForEnterprise( accountView.getId(), enterpriseId );
        if ( account == null ) {
            errors.reject( "account.notFound" );
        }

        if ( errors.hasErrors() ) {
            return;
        }

        account.setAccountNumber( accountView.getAccountNumber() );
        account.setName( accountView.getName() );
        account.setWebSite( accountView.getWebSite() );
        account.setDescription( accountView.getDescription() );
        account.setPhone( accountView.getPhone() );
        account.setFax( accountView.getFax() );
        if ( accountView.getAddressView() != null ) {
            Address address = new Address();
            address.setAddress1( accountView.getAddressView().getAddress1() );
            address.setAddress2( accountView.getAddressView().getAddress2() );
            address.setCity( accountView.getAddressView().getCity() );
            address.setState( accountView.getAddressView().getState() );
            address.setZipCode( accountView.getAddressView().getZipCode() );
            address.setCountry( accountView.getAddressView().getCountry() );
            account.setAddress( address );
        }
        accountService.getDefaultDao().saveEntity( account );
    }
}
