package efi.unleashed.workflow.impl;

import efi.unleashed.domain.Account;
import efi.unleashed.domain.Address;
import efi.unleashed.domain.Contact;
import efi.unleashed.domain.Enterprise;
import efi.unleashed.security.SessionUserContext;
import efi.unleashed.service.AccountService;
import efi.unleashed.service.ContactService;
import efi.unleashed.service.EnterpriseService;
import efi.unleashed.view.web.ContactAccountView;
import efi.unleashed.view.web.ContactView;
import efi.unleashed.workflow.ContactWorkflow;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

import javax.annotation.Resource;
import javax.validation.Valid;

@Service
public class ContactWorkflowImpl implements ContactWorkflow {

    public static final Logger LOGGER = Logger.getLogger( ContactWorkflowImpl.class.getName() );

    @Resource
    ContactService contactService;

    @Resource
    AccountService accountService;

    @Resource
    EnterpriseService enterpriseService;

    @Resource
    SessionUserContext sessionUserContext;

    @Override public ContactView findContact( Long id, Errors errors ) {

        if ( id == null ) {
            errors.reject( "contact.contactId.null" );
            return null;
        }

        Long enterpriseId = sessionUserContext.getEnterpriseId();
        if ( enterpriseId == null ) {
            errors.reject( "contact.add.enterpriseId.null" );
        }

        Contact contact = contactService.findContactForEnterprise( id, enterpriseId );
        if ( contact == null ) {
            errors.reject( "contact.notFound" );
        }

        if ( errors.hasErrors() ) {
            return null;
        }

        return new ContactView( contact );
    }

    @Valid
    @Override
    @Transactional
    public ContactView addContact( @Valid ContactView contactView, Errors errors ) {

        LOGGER.debug( "Attempting to add contact to database: " + contactView );

        Long enterpriseId = sessionUserContext.getEnterpriseId();
        Enterprise enterprise = enterpriseService.findByEnterpriseId( enterpriseId );
        if ( enterprise == null ) {
            errors.reject( "web.enterprise.not-found" );
        }

        //If an accountId is present on the contactView, look up the account and add it to the contact
        Account account = null;
        if ( contactView.getAccountId() != null ) {
            account = accountService.findAccountForEnterprise( contactView.getAccountId(), enterpriseId );
            if ( account == null ) {
                errors.reject( "account.notFound" );
            }
        }

        if ( errors.hasErrors() ) {
            return null;
        }

        Contact contact = new Contact();
        contact.setFirstName( contactView.getFirstName() );
        contact.setLastName( contactView.getLastName() );
        contact.setHomePhone( contactView.getHomePhone() );
        contact.setMobilePhone( contactView.getMobilePhone() );
        contact.setWorkPhone( contactView.getWorkPhone() );
        contact.setFax( contactView.getFax() );
        contact.setHomeEmail( contactView.getHomeEmail() );
        contact.setWorkEmail( contactView.getWorkEmail() );
        contact.setOtherEmail( contactView.getOtherEmail() );
        contact.setOptOut( contactView.getOptOut() );
        contact.setBirthday( contactView.getBirthday() );
        contact.setAnniversary( contactView.getAnniversary() );
        contact.setEnterprise( enterprise );
        contact.setAccount( account );

        if ( contactView.getAddressView() != null ) {
            Address address = new Address();
            address.setAddress1( contactView.getAddressView().getAddress1() );
            address.setAddress2( contactView.getAddressView().getAddress2() );
            address.setCity( contactView.getAddressView().getCity() );
            address.setState( contactView.getAddressView().getState() );
            address.setZipCode( contactView.getAddressView().getZipCode() );
            address.setCountry( contactView.getAddressView().getCountry() );
            contact.setAddress( address );
        }

        contactService.getDefaultDao().saveEntity( contact );
        ContactView response = new ContactView( contact );
        LOGGER.debug( response );
        return response;
    }

    @Override
    @Transactional
    @Valid
    public ContactView addAccountToContact( @Valid ContactAccountView contactAccountView, Errors errors ) {

        LOGGER.info( "Attempting to add existing contact to account: " + contactAccountView );

        Long enterpriseId = sessionUserContext.getEnterpriseId();

        Account account = accountService.findAccountForEnterprise( contactAccountView.getAccountId(), enterpriseId );
        if ( account == null ) {
            errors.reject( "account.accountId.null" );
        }

        Contact contact = contactService.findContactForEnterprise( contactAccountView.getContactId(), enterpriseId );
        if ( contact == null ) {
            errors.reject( "contact.notFound" );
        }

        if ( errors.hasErrors() ) {
            return null;
        }

        contact.setAccount( account );
        contactService.getDefaultDao().flushEntities();
        return new ContactView( contact );
    }

    @Valid
    @Transactional
    @Override
    public void updateContact( @Valid ContactView contactView, Errors errors ) {

        LOGGER.debug( "Attempting to update contact: " + contactView );

        Long enterpriseId = sessionUserContext.getEnterpriseId();
        Enterprise enterprise = enterpriseService.findByEnterpriseId( enterpriseId );
        if ( enterprise == null ) {
            errors.reject( "web.enterprise.not-found" );
        }
        //TODO Dan: Refactor to use web.enterprise.not-found validation message here
        Contact contact = contactService.findContactForEnterprise( contactView.getId(), enterpriseId );
        if ( contact == null ) {
            errors.reject( "contact.notFound" );
        }

        if ( errors.hasErrors() ) {
            return;
        }

        contact.setFirstName( contactView.getFirstName() );
        contact.setLastName( contactView.getLastName() );
        contact.setHomePhone( contactView.getHomePhone() );
        contact.setMobilePhone( contactView.getMobilePhone() );
        contact.setWorkPhone( contactView.getWorkPhone() );
        contact.setFax( contactView.getFax() );
        contact.setHomeEmail( contactView.getHomeEmail() );
        contact.setWorkEmail( contactView.getWorkEmail() );
        contact.setOtherEmail( contactView.getOtherEmail() );
        contact.setOptOut( contactView.getOptOut() );
        contact.setBirthday( contactView.getBirthday() );
        contact.setAnniversary( contactView.getAnniversary() );
        if ( contactView.getAddressView() != null ) {
            Address address = new Address();
            address.setAddress1( contactView.getAddressView().getAddress1() );
            address.setAddress2( contactView.getAddressView().getAddress2() );
            address.setCity( contactView.getAddressView().getCity() );
            address.setState( contactView.getAddressView().getState() );
            address.setZipCode( contactView.getAddressView().getZipCode() );
            address.setCountry( contactView.getAddressView().getCountry() );
            contact.setAddress( address );
        }
    }
}
