package efi.unleashed.workflow;

import efi.unleashed.domain.Account;
import efi.unleashed.view.web.AccountView;

import org.springframework.validation.Errors;

import java.util.List;

import javax.validation.Valid;

public interface AccountWorkflow {

    @Valid
    public AccountView findAccount(Long accountId, Errors errors);

    @Valid
    public AccountView addAccount(@Valid AccountView accountView, Errors errors);

    @Valid
    public void updateAccount(@Valid AccountView accountView, Errors errors);
}
