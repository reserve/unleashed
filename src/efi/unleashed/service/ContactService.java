package efi.unleashed.service;

import efi.platform.service.GenericService;
import efi.unleashed.domain.Contact;

import java.util.List;

/**
 * This interface is the service layer for managing user data
 */

public interface ContactService extends GenericService {

    List<Contact> findContactsByFirstName( String firstName, Long enterpriseId );

    Contact findContactForEnterprise( Long contactId, Long enterpriseId);

}
