package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.AuditDao;
import efi.unleashed.domain.AuditRevisionEntity;
import efi.unleashed.domain.AuditedEntityWrapper;
import efi.unleashed.service.AuditService;

import org.hibernate.envers.RevisionType;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

@Service
public class AuditServiceImpl extends GenericServiceImpl implements AuditService {

    @Resource
    AuditDao auditDao;
    
    @Override
    public GenericDao getDefaultDao() {

        return auditDao;
    }
    
    @Override
    public <T> List<AuditedEntityWrapper<T>> getAuditHistory( Class<T> clazz, Object entityId ) {

        List<AuditedEntityWrapper<T>> auditHistory = new ArrayList<AuditedEntityWrapper<T>>();

        List<Object[]> revisions = auditDao.getAuditHistory( clazz, entityId );
        for ( Object[] revision : revisions ) {
            AuditedEntityWrapper<T> wrapper = new AuditedEntityWrapper<T>();
            wrapper.setAuditedEntity( (T) revision[0] );
            AuditRevisionEntity revisionEntity = (AuditRevisionEntity) revision[1];
            wrapper.setUserId( revisionEntity.getUserId() );
            wrapper.setRevisionDate( revisionEntity.getRevisionDate() );
            wrapper.setRevisionType( (RevisionType) revision[2] );
            auditHistory.add( wrapper );
        }

        return auditHistory;
    }
}
