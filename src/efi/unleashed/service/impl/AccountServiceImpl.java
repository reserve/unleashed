package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.AccountDao;
import efi.unleashed.domain.Account;
import efi.unleashed.domain.Contact;
import efi.unleashed.service.AccountService;

import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

@Service
public class AccountServiceImpl extends GenericServiceImpl implements AccountService {

    @Resource
    AccountDao accountDao;

    @Override
    public GenericDao getDefaultDao() {

        return accountDao;
    }

    @Override public List<Account> findAccountByName( String name, Long enterpriseId ) {

        return accountDao.findAccountByName( name, enterpriseId );
    }

    @Override public Account findAccountForEnterprise( Long accountId, Long enterpriseId ) {

        return accountDao.findAccountForEnterprise( accountId, enterpriseId );
    }


}
