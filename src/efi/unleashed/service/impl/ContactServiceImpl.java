package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.ContactDao;
import efi.unleashed.domain.Contact;
import efi.unleashed.service.ContactService;

import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

@Service
public class ContactServiceImpl extends GenericServiceImpl implements ContactService {

    @Resource
    ContactDao contactDao;

    @Override
    public GenericDao getDefaultDao() {

        return contactDao;
    }

    public List<Contact> findContactsByFirstName( String firstName, Long enterpriseId ) {

        return contactDao.findContactsByFirstName( firstName, enterpriseId );
    }

    @Override public Contact findContactForEnterprise( Long contactId, Long enterpriseId ) {

        return contactDao.findContactForEnterprise( contactId, enterpriseId );
    }
}
