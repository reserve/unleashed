package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.UserDao;
import efi.unleashed.domain.ApplicationUser;
import efi.unleashed.service.UserService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl extends GenericServiceImpl implements UserService {

    @Resource
    UserDao userDao;

    @Override
    public GenericDao getDefaultDao() {
        return userDao;
    }
    
    @Override
    public ApplicationUser findByUsername( String username ) {
        return userDao.findByUsername( username );
    }

    @Override
    public ApplicationUser findByUserId( Long userId ) {
        return userDao.getEntity( ApplicationUser.class, userId );
    }
}
