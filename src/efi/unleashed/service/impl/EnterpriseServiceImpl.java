package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.EnterpriseDao;
import efi.unleashed.domain.Enterprise;
import efi.unleashed.service.EnterpriseService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class EnterpriseServiceImpl extends GenericServiceImpl implements EnterpriseService {

    @Resource
    EnterpriseDao enterpriseDao;

    @Override
    public GenericDao getDefaultDao() {

        return enterpriseDao;
    }

    @Override public Enterprise findByEnterpriseId( Long enterpriseId ) {

        return enterpriseDao.findEnterpriseById( enterpriseId );
    }

    @Override
    public Enterprise findByShortName( String shortName ) {

        return enterpriseDao.findByShortName( shortName );
    }
}
