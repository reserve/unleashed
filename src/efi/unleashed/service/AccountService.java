package efi.unleashed.service;


import efi.platform.service.GenericService;
import efi.unleashed.domain.Account;
import efi.unleashed.domain.Contact;

import java.util.List;

/**
 * This interface is the service layer for managing account data
 */

public interface AccountService extends GenericService {

    List<Account> findAccountByName( String name, Long enterpriseId );

    Account findAccountForEnterprise( Long accountId, Long enterpriseId);

}
