package efi.unleashed.service;

import efi.platform.service.GenericService;
import efi.unleashed.domain.AuditedEntityWrapper;

import java.util.List;

public interface AuditService extends GenericService {

    <T> List<AuditedEntityWrapper<T>> getAuditHistory( Class<T> clazz, Object entityId );
}
