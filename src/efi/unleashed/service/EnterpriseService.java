package efi.unleashed.service;

import efi.platform.service.GenericService;
import efi.unleashed.domain.Enterprise;

/**
 * This interface defines the service layer for most enterprise hierarchy interactions.
 */
public interface EnterpriseService extends GenericService {

    Enterprise findByEnterpriseId( Long enterpriseId );

    Enterprise findByShortName( String shortName );
}
