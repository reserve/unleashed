package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table( name = "USER_SITE_RIGHT" )
@org.hibernate.annotations.Table( appliesTo = "USER_SITE_RIGHT" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "USER_SITE_RIGHT_ID_SEQ" ) } )
public class UserSiteRight extends BaseEntity {

    private ApplicationUser user;
    private Site site;
    private Right right;

    @ManyToOne
    @JoinColumn( name = "APPLICATION_USER_ID", nullable = false )
    @ForeignKey( name = "FK_USER_SITE_RIGHT_TO_USER" )
    public ApplicationUser getUser() {

        return user;
    }

    public void setUser( ApplicationUser user ) {

        this.user = user;
    }

    @ManyToOne
    @JoinColumn( name = "SITE_ID", nullable = false )
    @ForeignKey( name = "FK_USER_SITE_RIGHT_TO_SITE" )
    public Site getSite() {

        return site;
    }

    public void setSite( Site site ) {

        this.site = site;
    }

    @ManyToOne
    @JoinColumn( name = "RIGHT_ID", nullable = false )
    @ForeignKey( name = "FK_USER_SITE_RIGHT_TO_RIGHT" )
    public Right getRight() {

        return right;
    }

    public void setRight( Right right ) {

        this.right = right;
    }
}
