package efi.unleashed.domain;

import org.hibernate.envers.RevisionType;

import java.util.Date;

/**
 * This class is a wrapper that contains the audited entity plus revision metadata.
 */
public class AuditedEntityWrapper<T> {

    private T auditedEntity;
    private Long userId;
    private Date revisionDate;
    private RevisionType revisionType;

    public T getAuditedEntity() {

        return auditedEntity;
    }

    public void setAuditedEntity( T auditedEntity ) {

        this.auditedEntity = auditedEntity;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId( Long userId ) {

        this.userId = userId;
    }

    public Date getRevisionDate() {

        return revisionDate;
    }

    public void setRevisionDate( Date revisionDate ) {

        this.revisionDate = revisionDate;
    }

    public RevisionType getRevisionType() {

        return revisionType;
    }

    public void setRevisionType( RevisionType revisionType ) {

        this.revisionType = revisionType;
    }
}
