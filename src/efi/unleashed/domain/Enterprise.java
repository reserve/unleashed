package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@NamedQueries( {
                       @NamedQuery( name = "Enterprise.findByShortName",
                                    query = "from Enterprise as e where lower(e.shortName) = lower(:shortName)" ),
                       @NamedQuery( name = "Enterprise.findById",
                                    query = "from Enterprise as e where e.id = :enterpriseId" ) } )

@Entity
@Table( name = "ENTERPRISE" )
@org.hibernate.annotations.Table( appliesTo = "ENTERPRISE" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "ENTERPRISE_ID_SEQ" ) } )

public class Enterprise extends BaseEntity {

    private String name;
    private String shortName;

    private Set<Division> divisions;

    public static Enterprise newInstance() {

        Enterprise enterprise = new Enterprise();
        enterprise.setDivisions( new HashSet<Division>() );
        return enterprise;
    }

    public Division addDivision( Division division ) {

        division.setEnterprise( this );
        getDivisions().add( division );
        return division;
    }

    @Column(name = "NAME", length = 128, nullable = false)
    public String getName() {

        return name;
    }

    public void setName( String name ) {

        this.name = name;
    }

    @Column(name = "SHORT_NAME", length = 64, nullable = false)
    public String getShortName() {

        return shortName;
    }

    public void setShortName( String shortName ) {

        this.shortName = shortName;
    }

    @OneToMany(targetEntity = Division.class, mappedBy = "enterprise", orphanRemoval = true)
    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    @ForeignKey(name = "FK_ENTERPRISE_TO_DIVISION")
    public Set<Division> getDivisions() {

        return divisions;
    }

    public void setDivisions( Set<Division> divisions ) {

        this.divisions = divisions;
    }

    @Override public String toString() {

        return "Enterprise {" +
               "name='" + name + '\'' +
               ", shortName='" + shortName + '\'' +
               "} " + super.toString();
    }
}
