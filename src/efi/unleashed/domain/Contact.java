package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@NamedQueries( {
                       @NamedQuery( name = "Contact.findByFirstName",
                                    query = "from Contact as c where c.firstName = :firstName and c.enterprise.id = :enterpriseId" ),
                       @NamedQuery( name = "Contact.findForEnterprise",
                                    query = "from Contact as c where c.id = :contactId and c.enterprise.id = :enterpriseId" ) } )
@Entity
@Table(name = "CONTACT")
@org.hibernate.annotations.Table(appliesTo = "CONTACT")
@GenericGenerator(name = "SequenceGenerator", strategy = "sequence",
                  parameters = { @Parameter(name = "sequence", value = "CONTACT_ID_SEQ") })
public class Contact extends BaseEntity {

    private String firstName;
    private String lastName;
    private String homePhone;
    private String mobilePhone;
    private String workPhone;
    private String fax;
    private String homeEmail;
    private String workEmail;
    private String otherEmail;
    private Boolean optOut;

    private LocalDate birthday;
    private LocalDate anniversary;

    private Account account;
    private Address address;
    private Enterprise enterprise;

    @Column(name = "HOME_EMAIL", length = 128)
    public String getHomeEmail() {

        return homeEmail;
    }

    public void setHomeEmail( String homeEmail ) {

        this.homeEmail = homeEmail;
    }
    @Column(name = "WORK_EMAIL", length = 128)
    public String getWorkEmail() {

        return workEmail;
    }

    public void setWorkEmail( String workEmail ) {

        this.workEmail = workEmail;
    }

    @Column(name = "OTHER_EMAIL", length = 128)
    public String getOtherEmail() {

        return otherEmail;
    }

    public void setOtherEmail( String otherEmail ) {

        this.otherEmail = otherEmail;
    }

    @Column(name = "OPT_OUT")
    public Boolean getOptOut() {

        return optOut;
    }

    public void setOptOut( Boolean optOut ) {

        this.optOut = optOut;
    }

    @Column(name = "FIRST_NAME", length = 32)
    public String getFirstName() {

        return firstName;
    }

    public void setFirstName( String firstName ) {

        this.firstName = firstName;
    }

    @Column(name = "LAST_NAME", length = 32, nullable = false)
    public String getLastName() {

        return lastName;
    }

    public void setLastName( String lastName ) {

        this.lastName = lastName;
    }

    @Column(name = "HOME_PHONE", length = 32)
    public String getHomePhone() {

        return homePhone;
    }

    public void setHomePhone( String homePhone ) {

        this.homePhone = homePhone;
    }

    @Column(name = "MOBILE_PHONE", length = 32)
    public String getMobilePhone() {

        return mobilePhone;
    }

    public void setMobilePhone( String mobilePhone ) {

        this.mobilePhone = mobilePhone;
    }

    @Column(name = "WORK_PHONE", length = 32)
    public String getWorkPhone() {

        return workPhone;
    }

    public void setWorkPhone( String workPhone ) {

        this.workPhone = workPhone;
    }

    @Column(name = "FAX", length = 32)
    public String getFax() {

        return fax;
    }

    public void setFax( String fax ) {

        this.fax = fax;
    }

    @Column(name = "BIRTHDAY")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    public LocalDate getBirthday() {

        return birthday;
    }

    public void setBirthday( LocalDate birthday ) {

        this.birthday = birthday;
    }

    @Column(name = "ANNIVERSARY")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    public LocalDate getAnniversary() {

        return anniversary;
    }

    public void setAnniversary( LocalDate anniversary ) {

        this.anniversary = anniversary;
    }

    @ManyToOne
    @JoinColumn( name = "ACCOUNT_ID" )
    @ForeignKey( name = "FK_CONTACT_TO_ACCOUNT" )
    public Account getAccount() {

        return account;
    }

    public void setAccount( Account account ) {

        this.account = account;
    }

    @Embedded
    public Address getAddress() {

        return address;
    }

    public void setAddress( Address address ) {

        this.address = address;
    }

    @ManyToOne
    @JoinColumn(name = "ENTERPRISE_ID")
    @ForeignKey(name = "FK_CONTACT_TO_ENTERPRISE")
    public Enterprise getEnterprise() {

        return enterprise;
    }

    public void setEnterprise( Enterprise enterprise ) {

        this.enterprise = enterprise;
    }

    @Override public String toString() {

        return "Contact {" +
               "firstName='" + firstName + '\'' +
               ", lastName='" + lastName + '\'' +
               ", homePhone='" + homePhone + '\'' +
               ", mobilePhone='" + mobilePhone + '\'' +
               ", workPhone='" + workPhone + '\'' +
               ", fax='" + fax + '\'' +
               ", email='" + homeEmail + '\'' +
               ", birthday=" + birthday +
               ", anniversary=" + anniversary +
               "} " + super.toString();
    }

}
