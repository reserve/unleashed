package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "Site.findForEnterprise",
                 query = "from Site as s where s.id = :siteId" +
                         " and s.property.division.enterprise.id = :enterpriseId" ) } )

@Audited
@Entity
@Table( name = "SITE" )
@org.hibernate.annotations.Table( appliesTo = "SITE" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "SITE_ID_SEQ" ) } )
public class Site extends BaseEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    private Property property;

    private Set<Location> locations;

    public static Site newInstance() {
        Site site = new Site();
        site.setLocations( new HashSet<Location>() );
        return site;
    }

    public Location addLocation( Location location ) {

        location.setSite( this );
        getLocations().add( location );
        return location;
    }

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "PROPERTY_ID", nullable = false)
    @ForeignKey(name = "FK_SITE_TO_PROPERTY")
    public Property getProperty() {

        return property;
    }

    public void setProperty( Property property ) {

        this.property = property;
    }

    @NotAudited
    @OneToMany( targetEntity = Location.class, mappedBy = "site", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_SITE_TO_LOCATION" )
    public Set<Location> getLocations() {

        return locations;
    }

    public void setLocations( Set<Location> locations ) {

        this.locations = locations;
    }

    @Override
    public String toString() {
        return "Site {" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
