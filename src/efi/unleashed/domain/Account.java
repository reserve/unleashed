package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;
import efi.unleashed.view.web.AccountView;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@NamedQueries( {
                       @NamedQuery( name = "Account.findByName",
                                    query = "from Account as a where a.name = :name and a.enterprise.id = :enterpriseId" ),
                       @NamedQuery( name = "Account.findForEnterprise",
                                    query = "from Account as a where a.id = :accountId and a.enterprise.id = :enterpriseId" ) } )

@Entity
@Table( name = "ACCOUNT" )
@org.hibernate.annotations.Table( appliesTo = "ACCOUNT" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "ACCOUNT_ID_SEQ" ) } )
public class Account extends BaseEntity {

    private String accountNumber;
    private String name;
    private String webSite;
    private String description;
    private String phone;
    private String fax;

    private Address address;
    private Enterprise enterprise;
    private Set<Contact> contacts;

    @Column( name = "ACCOUNT_NUMBER", length = 64 )
    public String getAccountNumber() {

        return accountNumber;
    }

    public void setAccountNumber( String accountNumber ) {

        this.accountNumber = accountNumber;
    }

    @Column( name = "NAME", length = 128 )
    public String getName() {

        return name;
    }

    public void setName( String name ) {

        this.name = name;
    }

    @Column( name = "WEB_SITE", length = 1024 )
    public String getWebSite() {

        return webSite;
    }

    public void setWebSite( String webSite ) {

        this.webSite = webSite;
    }

    @Column( name = "DESCRIPTION", length = 256 )
    public String getDescription() {

        return description;
    }

    public void setDescription( String description ) {

        this.description = description;
    }

    @Column( name = "PHONE", length = 32 )
    public String getPhone() {

        return phone;
    }

    public void setPhone( String phone ) {

        this.phone = phone;
    }

    @Column( name = "FAX", length = 32 )
    public String getFax() {

        return fax;
    }

    public void setFax( String fax ) {

        this.fax = fax;
    }

    @Embedded
    public Address getAddress() {

        return address;
    }

    public void setAddress( Address address ) {

        this.address = address;
    }

    @ManyToOne
    @JoinColumn( name = "ENTERPRISE_ID" )
    @ForeignKey( name = "FK_ACCOUNT_TO_ENTERPRISE" )
    public Enterprise getEnterprise() {

        return enterprise;
    }

    @OneToMany( targetEntity = Contact.class, mappedBy = "account", orphanRemoval = false )
    @ForeignKey( name = "FK_ACCOUNT_TO_CONTACT" )
    public Set<Contact> getContacts() {

        return contacts;
    }

    public void setContacts( Set<Contact> contacts ) {

        this.contacts = contacts;
    }

    public void setEnterprise( Enterprise enterprise ) {

        this.enterprise = enterprise;
    }

    @Override public String toString() {

        return "Account {" +
               "accountNumber='" + accountNumber + '\'' +
               ", name='" + name + '\'' +
               ", webSite='" + webSite + '\'' +
               ", description='" + description + '\'' +
               ", phone='" + phone + '\'' +
               "} " + super.toString();
    }
}
