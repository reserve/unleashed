package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "DIVISION")
@org.hibernate.annotations.Table(appliesTo = "DIVISION")
@GenericGenerator(name = "SequenceGenerator", strategy = "sequence",
                  parameters = { @Parameter(name = "sequence", value = "DIVISION_ID_SEQ") })

public class Division extends BaseEntity {

    private String name;

    private Enterprise enterprise;

    private Set<Property> properties;

    public static Division newInstance() {

        Division division = new Division();
        division.setProperties( new HashSet<Property>() );
        return division;
    }

    public Property addProperty( Property property ) {

        property.setDivision( this );
        getProperties().add( property );
        return property;
    }

    @Column(name = "NAME", length = 128, nullable = false)
    public String getName() {

        return name;
    }

    public void setName( String name ) {

        this.name = name;
    }

    @ManyToOne
    @JoinColumn(name = "ENTERPRISE_ID", nullable = false)
    @ForeignKey(name = "FK_DIVISION_TO_ENTERPRISE")
    public Enterprise getEnterprise() {

        return enterprise;
    }

    public void setEnterprise( Enterprise enterprise ) {

        this.enterprise = enterprise;
    }

    @OneToMany(targetEntity = Property.class, mappedBy = "division", orphanRemoval = true)
    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    @ForeignKey(name = "FK_DIVISION_TO_PROPERTY")
    public Set<Property> getProperties() {

        return properties;
    }

    public void setProperties( Set<Property> properties ) {

        this.properties = properties;
    }

    @Override public String toString() {

        return "Division {" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
