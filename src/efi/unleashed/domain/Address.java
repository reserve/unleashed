package efi.unleashed.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This is an embedded address table, which is used to avoid creating a separate address table in the database for
 * Accounts, Contacts etc..
 */

@Embeddable
public class Address {

    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zipCode;
    private String country;

    @Column( name = "ADDRESS_1", length = 256 )
    public String getAddress1() {

        return address1;
    }

    public void setAddress1( String address1 ) {

        this.address1 = address1;
    }

    @Column( name = "ADDRESS_2", length = 256 )
    public String getAddress2() {

        return address2;
    }

    public void setAddress2( String address2 ) {

        this.address2 = address2;
    }

    @Column( name = "CITY", length = 64 )
    public String getCity() {

        return city;
    }

    public void setCity( String city ) {

        this.city = city;
    }

    @Column( name = "STATE", length = 64 )
    public String getState() {

        return state;
    }

    public void setState( String state ) {

        this.state = state;
    }

    @Column( name = "ZIP_CODE", length = 32 )
    public String getZipCode() {

        return zipCode;
    }

    public void setZipCode( String zipCode ) {

        this.zipCode = zipCode;
    }

    @Column( name = "COUNTRY", length = 128 )
    public String getCountry() {

        return country;
    }

    public void setCountry( String country ) {

        this.country = country;
    }
}
