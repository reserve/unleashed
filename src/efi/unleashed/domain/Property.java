package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table( name = "PROPERTY" )
@org.hibernate.annotations.Table( appliesTo = "PROPERTY" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "PROPERTY_ID_SEQ" ) } )

public class Property extends BaseEntity {

    private String name;

    private Division division;

    private Set<Site> sites;

    public static Property newInstance() {

        Property property = new Property();
        property.setSites( new HashSet<Site>() );
        return property;
    }

    public Site addSite( Site site ) {

        site.setProperty( this );
        getSites().add( site );
        return site;
    }

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {

        return name;
    }

    public void setName( String name ) {

        this.name = name;
    }

    @ManyToOne
    @JoinColumn( name = "DIVISION_ID", nullable = false )
    @ForeignKey( name = "FK_PROPERTY_TO_DIVISION" )
    public Division getDivision() {

        return division;
    }

    public void setDivision( Division division ) {

        this.division = division;
    }

    @OneToMany( targetEntity = Site.class, mappedBy = "property", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_PROPERTY_TO_SITE" )
    public Set<Site> getSites() {

        return sites;
    }

    public void setSites( Set<Site> sites ) {

        this.sites = sites;
    }

    @Override public String toString() {

        return "Property {" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
