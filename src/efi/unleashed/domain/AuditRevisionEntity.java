package efi.unleashed.domain;

import efi.unleashed.audit.AuditRevisionListener;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class is used to store additional application-specific data with each Envers revision.
 */
@Entity
@Table( name = "AUDIT_REVISION_INFO" )
@org.hibernate.annotations.Table( appliesTo = "AUDIT_REVISION_INFO" )
@RevisionEntity( AuditRevisionListener.class )
public class AuditRevisionEntity extends DefaultRevisionEntity {

    private Long userId;

    @Column( name = "USER_ID" )
    public Long getUserId() {
        return userId;
    }

    public void setUserId( Long userId ) {

        this.userId = userId;
    }
}
