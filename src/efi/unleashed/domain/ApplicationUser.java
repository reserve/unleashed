package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * This class represents a user that can authenticate to the application,
 * either for using the web application or to send secured REST web service requests.
 */
@NamedQueries( {
    @NamedQuery( name = "ApplicationUser.findByUsername",
                 query = "from ApplicationUser as user where user.username = :username" ) } )

@Entity
@Table( name = "APPLICATION_USER" )
@org.hibernate.annotations.Table( appliesTo = "APPLICATION_USER" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "APPLICATION_USER_ID_SEQ" ) } )
public class ApplicationUser extends BaseEntity {

    @NotNull
    @Length( max = 32 )
    private String username;

    @Length( max = 64 )
    private String password;

    @Length( max = 32 )
    private String firstName;

    @NotNull
    @Length( max = 32 )
    private String lastName;

    @NotNull
    @Length( max = 4 )
    private String passcode;

    private boolean isActive = true;

    @NotNull
    private Role role;

    @NotNull
    private Enterprise primaryEnterprise;

    @Column( name = "USERNAME", length = 32, nullable = false )
    public String getUsername() {
        return username;
    }

    public void setUsername( String username ) {
        this.username = username;
    }

    @Column( name = "PASSWORD", length = 64 )
    public String getPassword() {
        return password;
    }

    public void setPassword( String password ) {
        this.password = password;
    }

    @Column( name = "FIRST_NAME", length = 32 )
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName( String firstName ) {
        this.firstName = firstName;
    }

    @Column( name = "LAST_NAME", length = 32, nullable = false )
    public String getLastName() {
        return lastName;
    }

    public void setLastName( String lastName ) {
        this.lastName = lastName;
    }

    @Column( name = "PASSCODE", length = 4, nullable = false )
    public String getPasscode() {
        return passcode;
    }

    public void setPasscode( String passcode ) {
        this.passcode = passcode;
    }

    @Column( name = "IS_ACTIVE", length = 1, nullable = false )
    @Type( type = "yes_no" )
    public boolean isActive() {
        return isActive;
    }

    public void setActive( boolean active ) {
        isActive = active;
    }

    @ManyToOne
    @JoinColumn( name = "ROLE_ID", nullable = false )
    @ForeignKey( name = "FK_USER_TO_ROLE" )
    public Role getRole() {
        return role;
    }

    public void setRole( Role role ) {
        this.role = role;
    }

    @ManyToOne( fetch = FetchType.EAGER )
    @JoinColumn( name = "PRIMARY_ENTERPRISE_ID", nullable = false )
    @ForeignKey( name = "FK_USER_TO_ENTERPRISE" )
    public Enterprise getPrimaryEnterprise() {

        return primaryEnterprise;
    }

    public void setPrimaryEnterprise( Enterprise primaryEnterprise ) {

        this.primaryEnterprise = primaryEnterprise;
    }

    @Override public String toString() {
        return "ApplicationUser {" +
               "firstName='" + firstName + '\'' +
               ", isActive=" + isActive +
               ", lastName='" + lastName + '\'' +
               ", passcode=" + passcode +
               ", password='" + password + '\'' +
               ", username='" + username + '\'' +
               "} " + super.toString();
    }

    @Transient
    public String getDisplayName() {
        StringBuilder builder = new StringBuilder();
        if ( StringUtils.isNotBlank( lastName ) ) {
            builder.append( lastName );
        }
        if ( builder.length() > 0 && StringUtils.isNotBlank( firstName ) ) {
            builder.append( ", " );
        }
        if ( StringUtils.isNotBlank( firstName ) ) {
            builder.append( firstName );
        }
        return builder.toString();
    }
}
