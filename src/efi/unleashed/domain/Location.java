package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table( name = "LOCATION" )
@org.hibernate.annotations.Table( appliesTo = "LOCATION" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "LOCATION_ID_SEQ" ) } )

public class Location extends BaseEntity {

    private String name;

    private Site site;

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {

        return name;
    }

    public void setName( String name ) {

        this.name = name;
    }

    @ManyToOne
    @JoinColumn( name = "SITE_ID", nullable = false )
    @ForeignKey( name = "FK_LOCATION_TO_SITE" )
    public Site getSite() {

        return site;
    }

    public void setSite( Site site ) {

        this.site = site;
    }

    @Override public String toString() {

        return "Location {" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
