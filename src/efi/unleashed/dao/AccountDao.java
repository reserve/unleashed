package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.Account;

import java.util.List;

public interface AccountDao extends GenericDao {

    List<Account> findAccountByName( String name, Long enterpriseId );

    Account findAccountForEnterprise( Long accountId, Long enterpriseId );

}
