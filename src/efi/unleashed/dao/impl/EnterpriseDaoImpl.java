package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.EnterpriseDao;
import efi.unleashed.domain.Enterprise;
import efi.unleashed.domain.Site;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EnterpriseDaoImpl extends GenericDaoImpl implements EnterpriseDao {

    @Autowired
    public EnterpriseDaoImpl( SessionFactory sessionFactory ) {

        setSessionFactory( sessionFactory );
    }

    @Override public Enterprise findEnterpriseById( Long enterpriseId ) {

        return (Enterprise) findEntity( "Enterprise.findById", "enterpriseId", enterpriseId );
    }

    @Override
    public Enterprise findByShortName( String shortName ) {

        return (Enterprise) findEntity( "Enterprise.findByShortName", "shortName", shortName );
    }

    @Override
    public Site findSiteForEnterprise( Long siteId, Long enterpriseId ) {

        return (Site) findEntity( "Site.findForEnterprise", "siteId", siteId, "enterpriseId", enterpriseId );
    }
}
