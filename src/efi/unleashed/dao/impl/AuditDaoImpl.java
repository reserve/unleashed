package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.AuditDao;

import org.hibernate.SessionFactory;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.exception.NotAuditedException;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AuditDaoImpl extends GenericDaoImpl implements AuditDao {

    @Autowired
    public AuditDaoImpl( SessionFactory sessionFactory ) {

        setSessionFactory( sessionFactory );
    }


    @SuppressWarnings( "unchecked" )
    @Override
    public <T> List<Object[]> getAuditHistory( Class<T> clazz, Object entityId )
            throws IllegalArgumentException, NotAuditedException, IllegalStateException {

        AuditReader reader = AuditReaderFactory.get( getSession() );
        return reader.createQuery()
                .forRevisionsOfEntity( clazz, false, true )
                .add( AuditEntity.id().eq( entityId ) )
                .getResultList();
    }
}
