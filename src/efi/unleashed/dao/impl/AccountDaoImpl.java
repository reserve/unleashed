package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.AccountDao;
import efi.unleashed.domain.Account;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountDaoImpl extends GenericDaoImpl implements AccountDao {

    @Autowired
    public AccountDaoImpl( SessionFactory sessionFactory ) {

        setSessionFactory( sessionFactory );
    }


    @Override
    public List<Account> findAccountByName( String name, Long enterpriseId ) {

        return findEntities( Account.class,
                             "Account.findByName", "name", name, "enterpriseId", enterpriseId );
    }

    @Override public Account findAccountForEnterprise( Long accountId, Long enterpriseId ) {

        return (Account) findEntity( "Account.findForEnterprise", "accountId", accountId, "enterpriseId", enterpriseId);
    }
}
