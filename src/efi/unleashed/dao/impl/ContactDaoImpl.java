package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.ContactDao;
import efi.unleashed.domain.Contact;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContactDaoImpl extends GenericDaoImpl implements ContactDao {

    @Autowired
    public ContactDaoImpl( SessionFactory sessionFactory ) {

        setSessionFactory( sessionFactory );
    }

    public List<Contact> findContactsByFirstName( String firstName, Long enterpriseId ) {

        return findEntities( Contact.class,
                             "Contact.findByFirstName",
                             "firstName",
                             firstName,
                             "enterpriseId",
                             enterpriseId );
    }

    public Contact findContactForEnterprise( Long contactId, Long enterpriseId ) {

        return (Contact) findEntity( "Contact.findForEnterprise",
                                     "contactId",
                                     contactId,
                                     "enterpriseId",
                                     enterpriseId );
    }

}
