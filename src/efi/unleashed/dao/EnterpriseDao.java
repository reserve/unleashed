package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.Enterprise;
import efi.unleashed.domain.Site;

/**
 * This interface defines the queries used to retrieve enterprise hierarchy domain entities.
 */
public interface EnterpriseDao extends GenericDao {

    Enterprise findEnterpriseById( Long enterpriseId );

    Enterprise findByShortName( String shortName );

    Site findSiteForEnterprise( Long siteId, Long enterpriseId );
}
