package efi.unleashed.dao;

import efi.platform.dao.GenericDao;

import org.hibernate.envers.exception.NotAuditedException;

import java.util.List;

public interface AuditDao extends GenericDao {

    /**
     * Returns a list of three-element arrays, containing:
     * <ol>
     * <li>the entity instance</li>
     * <li>the revision entity {@link efi.unleashed.domain.AuditRevisionEntity}</li>
     * <li>the type of the revision {@link org.hibernate.envers.RevisionType}</li>
     * </ol>
     */
    <T> List<Object[]> getAuditHistory( Class<T> clazz, Object entityId ) throws IllegalArgumentException,
                                                                                 NotAuditedException,
                                                                                 IllegalStateException;
}
