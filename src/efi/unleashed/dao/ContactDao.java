package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.Contact;

import java.util.List;

public interface ContactDao extends GenericDao {

    List<Contact> findContactsByFirstName( String firstName, Long enterpriseId );

    Contact findContactForEnterprise( Long contactId, Long enterpriseId );
}


