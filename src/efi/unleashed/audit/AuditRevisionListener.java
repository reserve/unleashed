package efi.unleashed.audit;

import efi.platform.util.SpringApplicationContext;
import efi.unleashed.domain.AuditRevisionEntity;
import efi.unleashed.security.SessionUserContext;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * This class is used to listen for audit changes from Envers to store the additional application-specific data.
 */
@Service
public class AuditRevisionListener implements RevisionListener {

    @Override
    public void newRevision( Object revisionEntity ) {

        if ( revisionEntity instanceof AuditRevisionEntity ) {
            AuditRevisionEntity entity = (AuditRevisionEntity) revisionEntity;
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if ( authentication == null || !authentication.isAuthenticated() ) {
                throw new RuntimeException( "Unauthenticated user." );
            }
            SessionUserContext userContext =
                    (SessionUserContext) SpringApplicationContext.getBean( "sessionUserContext" );
            entity.setUserId( userContext.getUserId() );
        }
    }
}
