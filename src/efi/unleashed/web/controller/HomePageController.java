package efi.unleashed.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This class handles page requests for URLs at the home page level.
 */
@Controller
@RequestMapping( value = HomePageController.HOME )
public class HomePageController {

    // Controller mapping name
    public static final String HOME = "web/home";

    // Request mapping names (please maintain alphabetic order)
    public static final String MAIN = "main";

    // Resulting view names (please maintain alphabetic order)
    public static final String HOME_MAIN = HOME + "/" + MAIN;

    @RequestMapping( value = MAIN, method = RequestMethod.GET )
    public String mainHomePage() {
        return HOME_MAIN;
    }
}
