package efi.unleashed.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This class handles page requests for URLs for all Tracers screens.
 */
@Controller
@RequestMapping( value = TracersController.TRACERS )
public class TracersController {

    // Controller mapping name
    public static final String TRACERS = "web/tracers";

    // Request mapping names (please maintain alphabetic order)
    public static final String TRACERS_MAIN = "tracersMain";

    // Resulting view names (please maintain alphabetic order)
    public static final String TRACERS_TRACERS_MAIN = TRACERS + "/" + TRACERS_MAIN;

    @RequestMapping( value = TRACERS_MAIN, method = RequestMethod.GET )
    public String tracersMainPage( ) {
        return TRACERS_TRACERS_MAIN;
    }
}