package efi.unleashed.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This class handles page requests for URLs for the Events module.
 */
@Controller
@RequestMapping( value = EventsController.EVENTS )
public class EventsController {

    // Controller mapping name
    public static final String EVENTS = "web/events";

    // Request mapping names (please maintain alphabetic order)
    public static final String EVENT_BOOK = "eventbook";

    // Resulting view names (please maintain alphabetic order)
    public static final String EVENTS_EVENT_BOOK = EVENTS + "/" + EVENT_BOOK;

    @RequestMapping( value = EVENT_BOOK, method = RequestMethod.GET )
    public String eventBookPage( ) {
        return EVENTS_EVENT_BOOK;
    }
}
