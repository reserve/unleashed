package efi.unleashed.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This class handles page requests for URLs for all Accounts screens.
 */
@Controller
@RequestMapping( value = AccountsController.ACCOUNTS )
public class AccountsController {
    // Controller mapping name
    public static final String ACCOUNTS = "web/accounts";

    // Request mapping names (please maintain alphabetic order)
    public static final String ACCOUNT_DETAILS = "accountDetails";
    public static final String ACCOUNTS_MAIN = "accountsMain";

    // Resulting view names (please maintain alphabetic order)
    public static final String ACCOUNTS_ACCOUNT_DETAILS = ACCOUNTS + "/" + ACCOUNT_DETAILS;
    public static final String ACCOUNTS_ACCOUNTS_MAIN = ACCOUNTS + "/" + ACCOUNTS_MAIN;

    @RequestMapping( value = ACCOUNT_DETAILS, method = RequestMethod.GET )
    public String accountDetailsPage( ) {
        return ACCOUNTS_ACCOUNT_DETAILS;
    }

    @RequestMapping( value = ACCOUNTS_MAIN, method = RequestMethod.GET )
    public String accountsMainPage( ) {
        return ACCOUNTS_ACCOUNTS_MAIN;
    }
}
