package efi.unleashed.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This class handles page requests for URLs for all Contacts screens.
 */
@Controller
@RequestMapping( value = ContactsController.CONTACTS )
public class ContactsController {
    // Controller mapping name
    public static final String CONTACTS = "web/contacts";

    // Request mapping names (please maintain alphabetic order)
    public static final String CONTACTS_MAIN = "contactsMain";

    // Resulting view names (please maintain alphabetic order)
    public static final String CONTACTS_CONTACTS_MAIN = CONTACTS + "/" + CONTACTS_MAIN;

    @RequestMapping( value = CONTACTS_MAIN, method = RequestMethod.GET )
    public String contactsMainPage( ) {
        return CONTACTS_CONTACTS_MAIN;
    }

}
