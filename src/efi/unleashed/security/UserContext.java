package efi.unleashed.security;

import java.io.Serializable;

/**
 * This class is used to define data that is available at various web-based scopes (request, session, etc.).
 */
public abstract class UserContext implements Serializable {

    public static final Long SYSTEM_USER_ID = 1L;
    public static final Long SYSTEM_ENTERPRISE_ID = 1L;

    private Long userId;
    private Long enterpriseId;

    public final void clear() {
        setUserId( SYSTEM_USER_ID );
        setEnterpriseId( null );
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId( Long userId ) {
        this.userId = userId;
        setEnterpriseId( null );
    }

    public Long getEnterpriseId() {

        return enterpriseId;
    }

    public void setEnterpriseId( Long enterpriseId ) {

        this.enterpriseId = enterpriseId;
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( !( o instanceof UserContext ) ) return false;

        UserContext that = (UserContext) o;

        if ( enterpriseId != null ? !enterpriseId.equals( that.enterpriseId ) : that.enterpriseId != null ) return false;
        if ( userId != null ? !userId.equals( that.userId ) : that.userId != null ) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + ( enterpriseId != null ? enterpriseId.hashCode() : 0 );
        return result;
    }

    @Override public String toString() {

        return "UserContext {" +
               "enterpriseId=" + enterpriseId +
               ", userId=" + userId +
               "} " + super.toString();
    }
}
