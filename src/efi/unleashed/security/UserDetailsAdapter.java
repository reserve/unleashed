package efi.unleashed.security;

import efi.unleashed.domain.ApplicationUser;
import efi.unleashed.domain.Enterprise;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * This class is used to interact with the Spring Security layer. It "adapts" the UserDetails API without our Hibernate
 * domain model needing to implement the interface. It can also expose additional attributes that may be needed by
 * Spring Security, such as a field to use for salting a password.
 * <p/>
 * See http://wheelersoftware.com/articles/spring-security-hash-salt-passwords.html
 */
public class UserDetailsAdapter extends User {

    private Enterprise enterprise;

    public UserDetailsAdapter( ApplicationUser applicationUser, Collection<? extends GrantedAuthority> authorities ) {

        super( applicationUser.getUsername(),
               applicationUser.getPassword(),
               applicationUser.isActive(),
               true,
               true,
               true,
               authorities );
        this.enterprise = applicationUser.getPrimaryEnterprise();
    }

    public Enterprise getEnterprise() {

        return enterprise;
    }
}
