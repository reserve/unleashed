package efi.unleashed.view.web;

import efi.unleashed.domain.Address;

import org.hibernate.validator.constraints.Length;

public class AddressView {

    @Length( max = 256 )
    private String address1;

    @Length( max = 256 )
    private String address2;

    @Length( max = 128 )
    private String city;

    @Length( max = 128 )
    private String state;

    @Length( max = 32 )
    private String zipCode;

    @Length( max = 128 )
    private String country;

    public AddressView() {

    }

    public AddressView( Address address ) {

        this.address1 = address.getAddress1();
        this.address2 = address.getAddress2();
        this.city = address.getCity();
        this.state = address.getState();
        this.zipCode = address.getZipCode();
        this.country = address.getCountry();
    }

    public String getAddress1() {

        return address1;
    }

    public void setAddress1( String address1 ) {

        this.address1 = address1;
    }

    public String getAddress2() {

        return address2;
    }

    public void setAddress2( String address2 ) {

        this.address2 = address2;
    }

    public String getCity() {

        return city;
    }

    public void setCity( String city ) {

        this.city = city;
    }

    public String getState() {

        return state;
    }

    public void setState( String state ) {

        this.state = state;
    }

    public String getZipCode() {

        return zipCode;
    }

    public void setZipCode( String zipCode ) {

        this.zipCode = zipCode;
    }

    public String getCountry() {

        return country;
    }

    public void setCountry( String country ) {

        this.country = country;
    }

    @Override
    public String toString() {

        return "AddressView {" +
               "address1='" + address1 + '\'' +
               ", address2='" + address2 + '\'' +
               ", city='" + city + '\'' +
               ", state='" + state + '\'' +
               ", zipCode='" + zipCode + '\'' +
               ", country='" + country + '\'' +
               "} " + super.toString();
    }
}
