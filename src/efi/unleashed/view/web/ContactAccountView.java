package efi.unleashed.view.web;

import javax.validation.constraints.NotNull;

/**
 * This view is used to associate a contact to an an account.
 */
public class ContactAccountView {

    @NotNull
    private Long contactId;

    @NotNull
    private Long accountId;

    public Long getContactId() {

        return contactId;
    }

    public void setContactId( Long contactId ) {

        this.contactId = contactId;
    }

    public Long getAccountId() {

        return accountId;
    }

    public void setAccountId( Long accountId ) {

        this.accountId = accountId;
    }

    @Override
    public String toString() {

        return "ContactAccountView {" +
               "contactId=" + contactId +
               ", accountId=" + accountId +
               "} " + super.toString();
    }
}
