package efi.unleashed.view.web;

import efi.platform.view.web.EntityView;
import efi.unleashed.domain.Account;
import efi.unleashed.domain.Contact;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

public class AccountView extends EntityView {

    @Length(max = 64)
    private String accountNumber;

    @NotEmpty
    @Length(max = 128)
    private String name;

    @Length(max = 1024)
    private String webSite;

    @Length(max = 256)
    private String description;

    @Length(max = 32)
    private String phone;

    @Length(max = 32)
    private String fax;

    private AddressView addressView;

    @Valid
    private List<ContactView> contacts;

    public AccountView() {

    }

    public AccountView( Account account ) {

        this.setId( account.getId() );
        this.accountNumber = account.getAccountNumber();
        this.name = account.getName();
        this.webSite = account.getWebSite();
        this.description = account.getDescription();
        this.phone = account.getPhone();
        this.fax = account.getFax();

        if ( account.getContacts() != null ) {
            this.contacts = new ArrayList<ContactView>();
            for ( Contact contact : account.getContacts() ) {
                this.contacts.add( new ContactView( contact ) );
            }
        }

        if ( account.getAddress() != null ) {
            AddressView addressView = new AddressView( account.getAddress() );
            this.addressView = addressView;
        }
    }

    public String getName() {

        return name;
    }

    public void setName( String name ) {

        this.name = name;
    }

    public String getWebSite() {

        return webSite;
    }

    public void setWebSite( String webSite ) {

        this.webSite = webSite;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription( String description ) {

        this.description = description;
    }

    public String getPhone() {

        return phone;
    }

    public void setPhone( String phone ) {

        this.phone = phone;
    }

    public String getFax() {

        return fax;
    }

    public void setFax( String fax ) {

        this.fax = fax;
    }

    public AddressView getAddressView() {

        return addressView;
    }

    public void setAddressView( AddressView addressView ) {

        this.addressView = addressView;
    }

    public List<ContactView> getContacts() {

        return contacts;
    }

    public void setContacts( List<ContactView> contacts ) {

        this.contacts = contacts;
    }

    public String getAccountNumber() {

        return accountNumber;
    }

    public void setAccountNumber( String accountNumber ) {

        this.accountNumber = accountNumber;
    }

    @Override
    public String toString() {

        return "AccountView {" +
               "accountNumber='" + accountNumber + '\'' +
               ", name='" + name + '\'' +
               ", webSite='" + webSite + '\'' +
               ", description='" + description + '\'' +
               ", phone='" + phone + '\'' +
               ", fax='" + fax + '\'' +
               ", AddressView=" + addressView +
               ", contacts=" + contacts +
               "} " + super.toString();
    }
}
