package efi.platform.view.web;

import efi.platform.domain.BaseEntity;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ext.JodaSerializers;
import org.joda.time.DateTime;

public class EntityView {
    
    private Long id;

    private DateTime lastUpdated;

    public EntityView() {}

    public EntityView( BaseEntity entity ) {
        this.id = entity.getId();
        this.lastUpdated = entity.getLastUpdated();
    }

    public Long getId() {
        return id;
    }
    public void setId( Long id ) {
        this.id = id;
    }

    public DateTime getLastUpdated() {
        return lastUpdated;
    }
    public void setLastUpdated( DateTime lastUpdated ) {
        this.lastUpdated = lastUpdated;
    }

    @Override public String toString() {
        return "EntityView {" +
               "id=" + id +
               ", lastUpdated=" + lastUpdated +
               '}';
    }
}
