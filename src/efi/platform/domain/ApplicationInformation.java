package efi.platform.domain;

/**
 * This class is used to hold application information, such as version numbers, build numbers, copyright years,
 * application name, etc.  Its properties are filled in when by both ant variables and application properties.
 */
public class ApplicationInformation {

    private String copyrightYears;
    private String version;
    private String buildNumber;

    public String getCopyrightYears() {
        return copyrightYears;
    }

    public void setCopyrightYears( String copyrightYears ) {
        this.copyrightYears = copyrightYears;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion( String version ) {
        this.version = version;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber( String buildNumber ) {
        this.buildNumber = buildNumber;
    }
}
