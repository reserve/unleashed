package efi.platform.core.hibernate;

import efi.platform.domain.LastUpdatable;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.Serializable;

/**
 * This class is used to update the last update date/time on an entity.
 */
public class LastUpdatedInterceptor extends EmptyInterceptor {

    @Override
    public boolean onFlushDirty( Object entity,
                                 Serializable id,
                                 Object[] currentState,
                                 Object[] previousState,
                                 String[] propertyNames,
                                 Type[] types ) {
        if ( entity instanceof LastUpdatable ) {
            for ( int i = 0; i < propertyNames.length; i++ ) {
                if ( LastUpdatable.LAST_UPDATED_PROPERTY_NAME.equals( propertyNames[i] ) ) {
                    currentState[i] = DateTime.now( DateTimeZone.UTC );
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean onSave( Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types ) {
        if ( entity instanceof LastUpdatable ) {
            for ( int i = 0; i < propertyNames.length; i++ ) {
                if ( LastUpdatable.LAST_UPDATED_PROPERTY_NAME.equals( propertyNames[i] ) ) {
                    state[i] = DateTime.now( DateTimeZone.UTC );
                    return true;
                }
            }
        }
        return false;
    }
}
