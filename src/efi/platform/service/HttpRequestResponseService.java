package efi.platform.service;

/**
 * This interface defines the methods a request/response communication service must implement.
 */
public interface HttpRequestResponseService {

    /**
     * Send an HTTP request via an XML request, a REST-like interaction.  Assumes no certificate verification.
     * @param xmlRequest The XML string containing the request.
     * @param requestUrl The URL string with which to connect.
     * @return An XML string containing the response.
     */
    String postXmlRequest( String xmlRequest, String requestUrl );

    /**
     * Send an HTTP request via an XML request, a REST-like interaction.
     * @param xmlRequest The XML string containing the request.
     * @param requestUrl The URL string with which to connect.
     * @param verifyCertificate If true, the service will verify that the remote end-point is valid for HTTPS.
     * @return An XML string containing the response.
     */
    String postXmlRequest( String xmlRequest, String requestUrl, boolean verifyCertificate );
}
