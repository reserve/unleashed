package efi.platform.service;

import efi.platform.dao.GenericDao;
import efi.platform.domain.BaseEntity;

import java.util.List;

/**
 * Services use DAOs and other services.  Services are where transactions are defined.
 * Each service may use one or more DAOs, so a default DAO is defined and a few DAO-level
 * methods are added so that simple DAO interactions can be performed without a lot of method chaining.
 */
public interface GenericService {

    GenericDao getDefaultDao();

    <T extends BaseEntity> T getEntity( Class<T> classObject, Long id );

    <T extends BaseEntity> List<T> loadAllEntities( Class<T> classObject );
}
