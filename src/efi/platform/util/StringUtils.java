package efi.platform.util;

/**
 * This class contains convenience methods that deal with Strings.
 */
public class StringUtils {

    private StringUtils() {}

    /**
     * Make the String value presentable to a user.
     * Capitalizes the string, and inserts a space before each upper case character. Thus "userId" becomes "User Id".
     * Also, converts an underscore into a space (and capitalizes the next word),
     * thus "user_id" also becomes "User Id".
     * <p/>
     * Thanks to TapestryInternalUtils.
     */
    public static String toUserPresentable( String value ) {

        StringBuilder builder = new StringBuilder( value.length() * 2 );
        char[] chars = value.toCharArray();
        boolean isAfterSpace = true;
        boolean nextNeedsUpperCase = true;

        for ( char ch : chars ) {
            if ( nextNeedsUpperCase ) {
                builder.append( Character.toUpperCase( ch ) );
                nextNeedsUpperCase = false;
                continue;
            }

            if ( ch == '_' ) {
                builder.append( ' ' );
                nextNeedsUpperCase = true;
                continue;
            }

            boolean isUpperCase = Character.isUpperCase( ch );
            if ( isUpperCase && !isAfterSpace ) {
                builder.append( ' ' );
            }

            builder.append( ch );
            isAfterSpace = isUpperCase;
        }

        return builder.toString();
    }

    /**
     * Make the Enum value presentable, thus "AN_ENUM_VALUE" becomes "An Enum Value".
     */
    public static String toUserPresentable( Enum value ) {
        return toUserPresentable( value.name().toLowerCase() );
    }
}
