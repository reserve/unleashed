package efi.platform.workflow;

import org.springframework.validation.Errors;

/**
 * This class is used to signal that validation errors occurred for scalar arguments to an @Valid annotated method.
 */
public class WorkflowValidationException extends WorkflowErrorsException {

    public WorkflowValidationException( String message ) {
        super( message );
    }

    public WorkflowValidationException( String message, Errors errors ) {
        super( message, errors );
    }
}
