package efi.platform.web.controller;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.MapBindingResult;

import java.util.HashMap;

/**
 * This class is used to provide a mechanism to record workflow errors in a manner similar to the way that
 * Spring MVC provides a binding result for UI form-based validation.
 */
public class WorkflowErrors extends MapBindingResult {

    public WorkflowErrors() {
        super( new HashMap(), "workflow" );
    }

    public WorkflowErrors( Model model ) {
        super( new HashMap(), "workflow" );
        model.addAttribute( "workflowErrors", this );
    }

    public WorkflowErrors( ModelMap model ) {
        super( new HashMap(), "workflow" );
        model.addAttribute( "workflowErrors", this );
    }
}
