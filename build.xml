<?xml version="1.0"?>
<project name="unleashed" basedir="." default="help">
    <property file="build-properties/${user.name}.properties"/>
    <property file="build-properties/common.properties"/>

    <property name="app.name" value="unleashed"/>
    <property name="deployed.app.dir" value="ROOT"/>
    <property name="deployed.app.context" value=""/>
    <property name="app.build_number" value="UNKNOWN"/>

    <property name="build-output.dir" value="build-output"/>
    <property name="classes.dir" value="${build-output.dir}/classes"/>
    <property name="cache.dir" value="${build-output.dir}/depcache"/>
    <property name="packages.dir" value="${build-output.dir}/packages"/>
    <property name="staging.dir" value="${build-output.dir}/staging"/>
    <property name="target.dir" value="${build-output.dir}/target"/>
    <property name="integrating.dir" value="${build-output.dir}/integrating"/>

    <property name="test-output.dir" value="${build-output.dir}/test"/>
    <property name="test-classes.dir" value="${test-output.dir}/classes"/>
    <property name="test-cache.dir" value="${test-output.dir}/depcache"/>

    <property name="dbscripts.dir" value="database/releases"/>
    <property name="dbpatches.dir" value="database/patches"/>
    <property name="dbtesting.dir" value="database/testing"/>
    <property name="lib.dir" value="lib"/>
    <property name="test-lib.dir" value="${lib.dir}/test-support"/>
    <property name="ci-lib.dir" value="${lib.dir}/ci-support"/>
    <property name="jsp-lib.dir" value="${lib.dir}/jsp-support"/>

    <property name="cobertura.dir" value="${ci-lib.dir}/cobertura"/>
    <property name="cobertura.ser" value="${build-output.dir}/cobertura.ser"/>
    <property name="instrumented.dir" value="${build-output.dir}/coberturaclasses"/>
    <property name="coveragereport.dir" value="${test-output.dir}/coverage"/>

    <property name="metadata.dir" value="metadata"/>
    <property name="src.dir" value="src"/>
    <property name="test-src.dir" value="test"/>
    <property name="web.dir" value="web"/>
    <property name="properties.dir" value="build-properties"/>
    <property name="schema.dir" value="schemas"/>

    <property name="dev-setup.dir" value="dev"/>

    <property name="war.file-name" value="${deployed.app.dir}.war"/>

    <!-- TODO Figure out how to support more than one application server type, e.g. jetty, jboss, tomcat -->

    <property name="tomcat.home" value="/usr/local/tomcat7"/>
    <property name="tomcat.version" value="7.0.25"/>
    <property name="tomcat.metadata" value="${metadata.dir}/tomcat-${tomcat.version}"/>
    <property name="tomcat.manager.url" value="http://localhost:8080/manager"/>
    <property name="tomcat.manager.username" value="admin"/>
    <property name="tomcat.manager.password" value="admin"/>
    <property name="tomcat.deploy.dir" value="${tomcat.home}/webapps"/>

    <property name="hibernate.dialect" value="org.hibernate.dialect.PostgreSQLDialect"/>
    <property name="hibernate.show_sql" value="false"/>
    <property name="database.driver_class" value="org.postgresql.Driver"/>
    <property name="database.url" value="jdbc:postgresql://localhost:5432/unleashed"/>
    <property name="database.username" value="unleashed"/>
    <property name="database.password" value="unleashed"/>

    <property name="dev.custom.database.url" value="jdbc:postgresql://localhost:5432/unleashed"/>
    <property name="stage.custom.database.url" value="jdbc:postgresql://localhost:5432/unleashed"/>

    <property environment="env"/>

    <property name="javac.debug" value="true"/>
    <property name="javac.optimize" value="true"/>
    <property name="javac.deprecation" value="false"/>
    <property name="javac.verbose" value="false"/>

    <!-- The compile jars are only needed to compile; package jars are used to compile and are deployed with the app. -->
    <path id="compile.classpath">
        <fileset dir="${lib.dir}/compile" includes="*.jar"/>
        <fileset dir="${lib.dir}/package" includes="*.jar"/>
    </path>

    <!-- Used for compiling and running unit tests. -->
    <path id="compile.tests.classpath">
        <path refid="compile.classpath"/>
        <fileset dir="${test-lib.dir}" includes="*.jar"/>
    </path>

    <!-- Used to pre-compile the JSPs for better performance. -->
    <path id="compile.jsp.classpath">
        <fileset dir="${jsp-lib.dir}" includes="*.jar"/>
    </path>

    <!--
    <taskdef classname="org.apache.jasper.JspC" name="jasper2">
        <classpath id="jspc.classpath">
            <path refid="compile.classpath"/>
            <path refid="compile.jsp.classpath"/>
        </classpath>
    </taskdef>
    -->

    <!-- Various classpaths used for continuous integration; code coverage and analysis. -->
    <path id="coburtura.classpath">
        <fileset dir="${cobertura.dir}" includes="*.jar"/>
    </path>

    <taskdef classpathref="coburtura.classpath" resource="tasks.properties"/>

    <path id="javancss.classpath">
        <fileset dir="${ci-lib.dir}/javancss" includes="*.jar"/>
    </path>

    <path id="pmd.classpath">
        <fileset dir="${ci-lib.dir}/pmd" includes="*.jar"/>
    </path>

    <!-- Tomcat Ant Tasks -->
    <path id="tomcat.ant.classpath">
        <pathelement path="${tomcat.home}/lib/catalina-ant.jar"/>
    </path>

    <taskdef name="deploy" classname="org.apache.catalina.ant.DeployTask" classpathref="tomcat.ant.classpath"/>
    <taskdef name="remove" classname="org.apache.catalina.ant.UndeployTask" classpathref="tomcat.ant.classpath"/>
    <taskdef name="reload" classname="org.apache.catalina.ant.ReloadTask" classpathref="tomcat.ant.classpath"/>
    <taskdef name="start" classname="org.apache.catalina.ant.StartTask" classpathref="tomcat.ant.classpath"/>
    <taskdef name="stop" classname="org.apache.catalina.ant.StopTask" classpathref="tomcat.ant.classpath"/>
    <taskdef name="list" classname="org.apache.catalina.ant.ListTask" classpathref="tomcat.ant.classpath"/>

    <target name="help">
        <echo message=""/>
        <echo message="${app.name} build file"/>
    </target>
    
    <target name="info" description="Information to guide build-properties creation">
        <echo message="My user.name variable is ${user.name}"/>
        <echo message="My build-properties file should be named ${user.name}.properties"/>
    </target>

    <target name="init" description="Initialize the runtime variables">
        <tstamp>
            <format property="build.display-datetime" pattern="yyyy-MM-dd HH:mm:ss z"/>
        </tstamp>
    </target>

    <target name="prepare" depends="init" description="Create the build output directories">
        <mkdir dir="${build-output.dir}"/>
        <mkdir dir="${classes.dir}"/>
        <mkdir dir="${cache.dir}"/>
        <mkdir dir="${packages.dir}"/>
        <mkdir dir="${staging.dir}"/>
        <mkdir dir="${target.dir}"/>
        <mkdir dir="${integrating.dir}"/>
        <mkdir dir="${test-output.dir}"/>
        <mkdir dir="${test-classes.dir}"/>
        <mkdir dir="${test-cache.dir}"/>
        <mkdir dir="${instrumented.dir}"/>
    </target>

    <target name="clean" description="Clean the build output directories" >
        <delete dir="${build-output.dir}"/>
        <delete file="junitvmwatcher*.properties"/>
    </target>

    <target name="compile" depends="prepare" description="Compile the main java source tree" >
        <depend srcdir="${src.dir}" destdir="${classes.dir}" cache="${cache.dir}" closure="true"/>
        <javac srcdir="${src.dir}" destdir="${classes.dir}"
            debug="${javac.debug}" optimize="${javac.optimize}"
            deprecation="${javac.deprecation}" verbose="${javac.verbose}"
            source="1.6">
            <classpath refid="compile.classpath"/>
        </javac>
        <!-- compile tests, too -->
        <depend srcdir="${test-src.dir}" destdir="${test-classes.dir}" cache="${test-cache.dir}" closure="true"/>
        <javac srcdir="${test-src.dir}" destdir="${test-classes.dir}"
            debug="${javac.debug}" optimize="${javac.optimize}"
            deprecation="${javac.deprecation}" verbose="${javac.verbose}"
            source="1.6">
            <classpath>
                <path refid="compile.tests.classpath"/>
                <path refid="compile.jsp.classpath"/>
                <path location="${classes.dir}"/>
            </classpath>
        </javac>
    </target>

    <filterset id="variables.to.replace">
        <filter token="APPNAME" value="${app.name}"/>
        <filter token="APPVERSION" value="${webapp.version}.${app.build_number}"/>
        <filter token="BUILDNUMBER" value="${app.build_number}"/>
        <filter token="COPYRIGHT-YEARS" value="${copyright.years}"/>
        <filter token="HIBERNATE-DIALECT" value="${hibernate.dialect}"/>
        <filter token="HIBERNATE-SHOW-SQL" value="${hibernate.show_sql}"/>
        <filter token="DB-DRIVERNAME" value="${database.driver_class}"/>
        <filter token="DB-URL" value="${database.url}"/>
        <filter token="DB-USERNAME" value="${database.username}"/>
        <filter token="DB-PASSWORD" value="${database.password}"/>
    </filterset>

    <!-- used for custom deployments -->
    <filterset id="dev.custom.variables.to.replace">
        <filter token="CUSTOM-DB-URL" value="${dev.custom.database.url}"/>
        <filter token="DEV-EMAIL" value="${dev.email.address}"/>
    </filterset>

    <filterset id="stage.custom.variables.to.replace">
        <filter token="CUSTOM-DB-URL" value="${stage.custom.database.url}"/>
    </filterset>

    <target name="gen-test-env" depends="compile" description="Prepare an environment for JUnit tests">
        <!-- Start with resources from the production environment.  Resources get filtered. -->
    	<mkdir dir="${target.dir}/filtered-resources"/>
        <copy todir="${target.dir}/filtered-resources" includeemptydirs="no">
            <fileset dir="${metadata.dir}/production-env/filtered-resources">
                <include name="**/*.properties"/>
                <include name="**/*.xml"/>
            </fileset>
            <filterset refid="variables.to.replace"/>
        </copy>
        <!-- Overwrite with deployment environment versions. -->
        <copy overwrite="true" todir="${target.dir}/filtered-resources">
            <fileset dir="${metadata.dir}/deploy-env/${dev-setup.dir}"/>
            <filterset refid="variables.to.replace"/>
        </copy>
        <!-- Finally, overwrite with test environment versions. -->
        <copy overwrite="true" todir="${target.dir}/filtered-resources">
            <fileset dir="${metadata.dir}/test-env/filtered-resources"/>
            <filterset refid="variables.to.replace"/>
        </copy>

        <!-- Bring over general, non-source files from production.  They do not get filtered. -->
        <mkdir dir="${target.dir}/general-resources"/>
        <copy todir="${target.dir}/general-resources">
            <fileset dir="${metadata.dir}/production-env/general-resources" includes="**/*.xml"/>
        </copy>
        <!-- Overwrite with test versions -->
        <copy overwrite="true" todir="${target.dir}/general-resources">
            <fileset dir="${metadata.dir}/test-env/general-resources"/>
        </copy>

        <!-- Bring over the WEB-INF files from the web directory. Split if some need filtering. -->
        <mkdir dir="${target.dir}/web"/>
        <mkdir dir="${target.dir}/web/WEB-INF"/>
        <copy todir="${target.dir}/web/WEB-INF">
            <fileset dir="${web.dir}/WEB-INF"/>
        </copy>
        <!-- Overwrite with test versions -->
        <!-- ...look in ${metadata.dir}/test-env/web/WEB-INF when there are any to copy -->

        <!-- Bring over the other web support files -->
        <mkdir dir="${target.dir}/web/help"/>
        <copy todir="${target.dir}/web/help">
            <fileset dir="${web.dir}/help"/>
        </copy>
        <!-- Overwrite with test versions -->
        <!-- ...look in ${metadata.dir}/test-env/web/help when there are any to copy -->

        <mkdir dir="${target.dir}/web/images"/>
        <copy todir="${target.dir}/web/images">
            <fileset dir="${web.dir}/images"/>
        </copy>
        <!-- Overwrite with test versions -->
        <!-- ...look in ${metadata.dir}/test-env/web/images when there are any to copy -->

        <mkdir dir="${target.dir}/web/scripts"/>
        <copy todir="${target.dir}/web/scripts">
            <fileset dir="${web.dir}/scripts"/>
        </copy>
        <!-- Overwrite with test versions -->
        <!-- ...look in ${metadata.dir}/test-env/web/scripts when there are any to copy -->

        <mkdir dir="${target.dir}/web/styles"/>
        <copy todir="${target.dir}/web/styles">
            <fileset dir="${web.dir}/styles"/>
        </copy>
        <!-- Overwrite with test versions -->
        <!-- ...look in ${metadata.dir}/test-env/web/styles when there are any to copy -->

        <!-- Copy any special test libraries -->
        <mkdir dir="${target.dir}/lib"/>
        <copy todir="${target.dir}/lib">
            <fileset dir="${lib.dir}/test-support" includes="*.jar"/>
        </copy>

        <!-- TODO precompile JSPs here... -->
    </target>

    <target name="regen-test-env" depends="clean, gen-test-env" description="Full clean and JUnit setup"/>

    <!-- remove HTML reports when running in CI tool -->
    <target name="coverage" depends="coverage-junit, coverage-reports, coverage-html-reports" description="Execute Code Coverage against test cases"/>

    <target name="coverage-junit" depends="gen-coverage-env, test"/>

    <target name="analysis" depends="cpd, pmd, javancss"/>

    <target name="cpd">
        <taskdef name="cpd" classname="net.sourceforge.pmd.cpd.CPDTask" classpathref="pmd.classpath"/>
        <cpd minimumTokenCount="75" outputFile="${build-output.dir}/cpd.xml" format="xml">
            <fileset dir="${src.dir}">
                <include name="**/*.java"/>
                <!-- a lot of false matches from all of the simple bean properties -->
                <exclude name="**/domain/*.java"/>
            </fileset>
        </cpd>
    </target>

    <target name="cpd-txt">
        <taskdef name="cpd" classname="net.sourceforge.pmd.cpd.CPDTask" classpathref="pmd.classpath"/>
        <cpd minimumTokenCount="75" outputFile="${build-output.dir}/cpd.txt">
            <fileset dir="${src.dir}">
                <include name="**/*.java"/>
                <!-- a lot of false matches from all of the simple bean properties -->
                <exclude name="**/domain/*.java"/>
            </fileset>
        </cpd>
    </target>

    <target name="pmd">
        <taskdef name="pmd" classname="net.sourceforge.pmd.ant.PMDTask" classpathref="pmd.classpath"/>
        <pmd rulesetfiles="basic,coupling,design,strings,imports,unusedcode">
            <formatter type="xml" toFile="${build-output.dir}/pmd_report.xml"/>
            <fileset dir="${src.dir}">
                <include name="**/*.java"/>
            </fileset>
        </pmd>
    </target>

    <target name="javancss">
        <taskdef name="javancss" classname="javancss.JavancssAntTask" classpathref="javancss.classpath"/>
        <javancss srcdir="${src.dir}" generateReport="true" format="xml" outputfile="${build-output.dir}/javancss.xml"
                  abortOnFail="false" includes="**/*.java" />
    </target>

    <target name="gen-coverage-env" depends="gen-test-env">
        <cobertura-instrument todir="${instrumented.dir}" datafile="${cobertura.ser}">
            <ignore regex="org.apache.log4j.*" />
            <fileset dir="${classes.dir}">
                <include name="**/*.class" />
                <!-- Ignore Exceptions and 3rd party stuff. -->
                <exclude name="efi/platform/core/*.class"/>
                <exclude name="efi/platform/core/hibernate/*.class"/>
                <exclude name="efi/platform/domain/hibernate/StringEnumUserType.class"/>
                <exclude name="efi/platform/workflow/*Exception.class"/>
                <exclude name="efi/platform/web/mapping/*.class"/>
                <!-- Ignore until we can unit test controllers. -->
                <exclude name="efi/platform/web/controller/*.class"/>
                <exclude name="efi/unleashed/web/controller/*.class"/>
            </fileset>
        </cobertura-instrument>
    </target>

    <target name="coverage-reports" description="Code Coverage reports">
        <mkdir dir="${coveragereport.dir}"/>
        <cobertura-report datafile="${cobertura.ser}" format="xml" destdir="${coveragereport.dir}" srcdir="${src.dir}" />
    </target>

    <target name="coverage-html-reports" description="Code Coverage HTML reports">
        <mkdir dir="${coveragereport.dir}"/>
        <cobertura-report datafile="${cobertura.ser}" format="html" destdir="${coveragereport.dir}" srcdir="${src.dir}" />
        <echo message="See coverage report in ${coveragereport.dir}"/>
    </target>

    <target name="test" depends="gen-test-env" description="Runs JUnit tests">
        <!-- Check that JUnit jars are in $ANT_HOME/lib -->
        <available classname="junit.framework.TestCase" property="junit.present"/>
        <fail unless="junit.present"
            message="Please copy ${lib.dir}/test-support/junit*.jar into ${env.ANT_HOME}/lib"/>

        <mkdir dir="${test-output.dir}/data"/>
        <junit printsummary="no" fork="on" forkmode="once"
            errorProperty="test.failed" failureProperty="test.failed" dir="${target.dir}">
            <jvmarg value="-server"/>

            <sysproperty key="net.sourceforge.cobertura.datafile" file="${cobertura.ser}" />
            <classpath>
                <path location="${instrumented.dir}"/>
                <path location="${classes.dir}"/>
                <path location="${test-classes.dir}"/>
                <path location="${target.dir}/filtered-resources"/>
                <path location="${target.dir}/web"/>
                <path location="${target.dir}/general-resources"/>
                <fileset dir="${target.dir}/lib" includes="*.jar"/>
                <fileset dir="${target.dir}/lib" includes="*.zip"/>
                <path refid="compile.tests.classpath"/>
                <path refid="compile.jsp.classpath"/>
                <path refid="coburtura.classpath"/>
            </classpath>
            <formatter type="xml"/>
            <formatter type="brief" usefile="false"/>
            <batchtest todir="${test-output.dir}/data" if="testcase">
                <fileset dir="${test-classes.dir}">
                    <include name="**/*${testcase}*"/>
                    <exclude name="**/*TestCase.class"/>
                    <exclude name="**/*$*.class"/>
                </fileset>
            </batchtest>
            <batchtest todir="${test-output.dir}/data" unless="testcase">
                <fileset dir="${test-classes.dir}">
                    <include name="**/*Test.class*"/>
                    <exclude name="**/*WebTest.class"/>
                </fileset>
            </batchtest>
        </junit>

        <fail if="test.failed">
          Unit tests failed. For error messages, check the log files in
          ${test-output.dir}/data or run "ant test-reports"
          to generate reports at ${test-output.dir}/reports.</fail>
    </target>

    <target name="remove-test-results" description="Clear out the previous unit test data and results" >
        <delete dir="${test-output.dir}/data"/>
        <delete dir="${test-output.dir}/reports"/>
        <delete dir="${test-output.dir}/coverage"/>
    </target>

    <target name="fresh-test" depends="remove-test-results, test" description="Clear old test data, then run unit tests"/>

    <target name="fresh-coverage" depends="remove-test-results, coverage" description="Clear old test data, then run unit tests and coverage"/>

    <target name="test-reports" description="Generate test reports">
        <mkdir dir="${test-output.dir}/reports"/>
        <junitreport todir="${test-output.dir}">
            <fileset dir="${test-output.dir}/data">
                <include name="TEST-*.xml"/>
            </fileset>
            <report format="frames" todir="${test-output.dir}/reports"/>
        </junitreport>
    </target>

    <!-- Integration targets begin here -->

    <target name="int-package" depends="gen-test-env" description="Package for integration deployment">
        <copy overwrite="true" todir="${target.dir}/filtered-resources">
            <fileset dir="${metadata.dir}/deploy-env/int"/>
        </copy>
        <copy todir="${classes.dir}" includeemptydirs="no">
            <fileset dir="${instrumented.dir}"/>
        </copy>
        <war destfile="${target.dir}/${war.file-name}"
            webxml="${target.dir}/web/WEB-INF/web.xml">
            <lib dir="${cobertura.dir}">
                <exclude name="log4j*jar"/>
            </lib>
            <classes dir="${classes.dir}"/>
            <classes dir="${target.dir}/filtered-resources"/>
            <classes dir="${target.dir}/general-resources"/>
            <classes dir="${test-classes.dir}"/>
            <lib dir="${lib.dir}/package"/>
            <lib dir="${test-lib.dir}">
                <include name="hsqldb.jar"/>
            </lib>
            <fileset dir="${target.dir}/web">
                <exclude name="**/web.xml"/>
            </fileset>
            <manifest>
                <attribute name="Created-By" value="${user.name}"/>
                <attribute name="Build-Date" value="${build.display-datetime}"/>
                <attribute name="Version" value="${webapp.version}"/>
                <attribute name="Build-Number" value="${app.build_number}"/>
            </manifest>
        </war>
    </target>

    <target name="integrate" depends="integrate-tar" description="Create installable tar file for continuous integration">
        <!-- Add auto deploy script here when needed. -->
    </target>
    
    <target name="integrate-tar" depends="int-package" description="Create installable tar files">
        <property name="install-prefix" value="tomcat7"/>
        
        <tar destfile="${integrating.dir}/int-${app.name}-install.tar" longfile="gnu">
            <tarfileset dir="${target.dir}" prefix="${install-prefix}/webapps">
                <include name="${war.file-name}"/>
            </tarfileset>
        </tar>
    </target>

    <target name="int-deploy" depends="gen-test-env" description="Deploy the test environment for development">
        <copy todir="${tomcat.deploy.dir}/${deployed.app.dir}" preservelastmodified="true">
            <fileset dir="${target.dir}/web"/>
        </copy>
        <copy todir="${tomcat.deploy.dir}/${deployed.app.dir}/WEB-INF/classes" preservelastmodified="true">
            <fileset dir="${classes.dir}"/>
            <fileset dir="${target.dir}/filtered-resources"/>
            <fileset dir="${target.dir}/general-resources"/>
            <fileset dir="${test-classes.dir}"/>
        </copy>
        <copy todir="${tomcat.deploy.dir}/${deployed.app.dir}/WEB-INF/classes" overwrite="true">
            <fileset dir="${metadata.dir}/deploy-env/dev" includes="log4j.properties"/>
        </copy>
        <copy todir="${tomcat.deploy.dir}/${deployed.app.dir}/WEB-INF/lib">
            <fileset dir="${lib.dir}/package"/>
        </copy>
        <copy todir="${tomcat.deploy.dir}/${deployed.app.dir}/WEB-INF/lib">
            <fileset dir="${target.dir}/lib" excludes="log4j*jar"/>
        </copy>
    </target>

    <!-- Production targets begin here -->

    <target name="gen-prod-env" depends="compile" description="Prepare an environment for packaging and deployment">
    	<mkdir dir="${packages.dir}/filtered-resources"/>
        <copy todir="${packages.dir}/filtered-resources" includeemptydirs="no">
            <fileset dir="${metadata.dir}/production-env/filtered-resources">
                <include name="**/*.properties"/>
                <include name="**/*.xml"/>
            </fileset>
            <filterset refid="variables.to.replace"/>
        </copy>

        <mkdir dir="${packages.dir}/general-resources"/>
        <copy todir="${packages.dir}/general-resources">
            <fileset dir="${metadata.dir}/production-env/general-resources" includes="**/*.xml"/>
        </copy>

        <mkdir dir="${packages.dir}/web"/>
        <copy todir="${packages.dir}/web" includeemptydirs="no">
            <fileset dir="${web.dir}">
                <include name="**"/>
                <exclude name="**/classes/**"/>
                <exclude name="**/help/**"/>
                <exclude name="**/images/**"/>
            	<exclude name="**/scripts/**"/>
            	<exclude name="**/styles/**"/>
            </fileset>
            <filterset refid="variables.to.replace"/>
        </copy>
        <copy todir="${packages.dir}/web" includeemptydirs="no">
            <fileset dir="${web.dir}">
                <include name="**/help/**"/>
                <include name="**/images/**"/>
            	<include name="**/scripts/**"/>
            	<include name="**/styles/**"/>
            </fileset>
        </copy>

        <!-- TODO precompile the JSPs here -->
    </target>

    <target name="package" depends="gen-prod-env" description="Package up deployable items for Tomcat">
        <!-- First for dev -->
        <copy overwrite="true" todir="${packages.dir}/filtered-resources">
            <fileset dir="${metadata.dir}/deploy-env/dev"/>
            <filterset refid="stage.custom.variables.to.replace"/>
        </copy>
        <war destfile="${packages.dir}/dev-${war.file-name}"
            webxml="${packages.dir}/web/WEB-INF/web.xml">
            <classes dir="${classes.dir}"/>
            <classes dir="${packages.dir}/filtered-resources"/>
            <classes dir="${packages.dir}/general-resources"/>
            <lib dir="${lib.dir}/package"/>
            <fileset dir="${packages.dir}/web">
                <exclude name="**/web.xml"/>
            </fileset>
            <manifest>
                <attribute name="Created-By" value="${user.name}"/>
                <attribute name="Build-Date" value="${build.display-datetime}"/>
                <attribute name="Version" value="${webapp.version}"/>
                <attribute name="Build-Number" value="${app.build_number}"/>
            </manifest>
        </war>

        <!-- test -->
        <copy overwrite="true" todir="${packages.dir}/filtered-resources">
            <fileset dir="${metadata.dir}/deploy-env/test"/>
        </copy>
        <war destfile="${packages.dir}/test-${war.file-name}"
            webxml="${packages.dir}/web/WEB-INF/web.xml">
            <classes dir="${classes.dir}"/>
            <classes dir="${packages.dir}/filtered-resources"/>
            <classes dir="${packages.dir}/general-resources"/>
            <lib dir="${lib.dir}/package"/>
            <fileset dir="${packages.dir}/web">
                <exclude name="**/web.xml"/>
            </fileset>
            <manifest>
                <attribute name="Created-By" value="${user.name}"/>
                <attribute name="Build-Date" value="${build.display-datetime}"/>
                <attribute name="Version" value="${webapp.version}"/>
                <attribute name="Build-Number" value="${app.build_number}"/>
            </manifest>
        </war>

        <!-- Finally, for production -->
        <copy overwrite="true" todir="${packages.dir}/filtered-resources">
            <fileset dir="${metadata.dir}/deploy-env/prod"/>
        </copy>
        <war destfile="${packages.dir}/prod-${war.file-name}"
            webxml="${packages.dir}/web/WEB-INF/web.xml">
            <classes dir="${classes.dir}"/>
            <classes dir="${packages.dir}/filtered-resources"/>
            <classes dir="${packages.dir}/general-resources"/>
            <lib dir="${lib.dir}/package"/>
            <fileset dir="${packages.dir}/web">
                <exclude name="**/web.xml"/>
            </fileset>
            <manifest>
                <attribute name="Created-By" value="${user.name}"/>
                <attribute name="Build-Date" value="${build.display-datetime}"/>
                <attribute name="Version" value="${webapp.version}"/>
                <attribute name="Build-Number" value="${app.build_number}"/>
            </manifest>
        </war>
    </target>

    <target name="setup-for-deploy" depends="gen-prod-env" description="Set up for local development">
        <copy todir="${packages.dir}/filtered-resources" includeemptydirs="true" overwrite="yes">
            <fileset dir="${metadata.dir}/deploy-env/${dev-setup.dir}"/>
            <filterset refid="dev.custom.variables.to.replace"/>
        </copy>
    </target>

    <target name="deploy" depends="setup-for-deploy" description="Deploy items to Tomcat">
        <copy todir="${tomcat.deploy.dir}/${deployed.app.dir}" preservelastmodified="true">
            <fileset dir="${packages.dir}/web"/>
        </copy>
        <copy todir="${tomcat.deploy.dir}/${deployed.app.dir}/WEB-INF/classes" preservelastmodified="true">
            <fileset dir="${classes.dir}"/>
            <fileset dir="${packages.dir}/filtered-resources"/>
            <fileset dir="${packages.dir}/general-resources"/>
        </copy>
        <copy todir="${tomcat.deploy.dir}/${deployed.app.dir}/WEB-INF/lib">
            <fileset dir="${lib.dir}/package"/>
        </copy>
    </target>

    <target name="undeploy" description="Deletes the application from Tomcat">
        <delete dir="${tomcat.deploy.dir}/${deployed.app.dir}"/>
    </target>

    <target name="install" description="Install application in Tomcat" depends="package">
        <deploy url="${tomcat.manager.url}"
            username="${tomcat.manager.username}"
            password="${tomcat.manager.password}"
            path="/${deployed.app.context}"
            war="file:${packages.dir}/${war.file-name}"/>
    </target>

    <target name="remove" description="Remove application from Tomcat">
        <remove url="${tomcat.manager.url}"
            username="${tomcat.manager.username}"
            password="${tomcat.manager.password}"
            path="/${deployed.app.context}"/>
    </target>

    <target name="reload" description="Reload application in Tomcat" depends="deploy">
        <reload url="${tomcat.manager.url}"
            username="${tomcat.manager.username}"
            password="${tomcat.manager.password}"
            path="/${deployed.app.context}"/>
    </target>

    <target name="int-reload" description="Reload application in Tomcat" depends="int-deploy">
        <reload url="${tomcat.manager.url}"
            username="${tomcat.manager.username}"
            password="${tomcat.manager.password}"
            path="/${deployed.app.context}"/>
    </target>

    <target name="start" description="Start Tomcat application">
        <start url="${tomcat.manager.url}"
            username="${tomcat.manager.username}"
            password="${tomcat.manager.password}"
            path="/${deployed.app.context}"/>
    </target>

    <target name="stop" description="Stop Tomcat application">
        <stop url="${tomcat.manager.url}"
            username="${tomcat.manager.username}"
            password="${tomcat.manager.password}"
            path="/${deployed.app.context}"/>
    </target>

    <target name="list" description="List Tomcat applications">
        <list url="${tomcat.manager.url}"
            username="${tomcat.manager.username}"
            password="${tomcat.manager.password}"/>
    </target>

    <target name="stage" depends="clean,stage-tar" description="Create installable tar files from a clean build"/>

    <target name="stage-tar" depends="package" description="Create installable tar files">
        <property name="install-prefix" value="tomcat7"/>

        <delete file="${packages.dir}/${war.file-name}"/>
        <copy file="${packages.dir}/dev-${war.file-name}" tofile="${packages.dir}/${war.file-name}"/>
        <tar destfile="${staging.dir}/dev-${app.name}-install.tar" longfile="gnu">
            <tarfileset dir="${packages.dir}" prefix="${install-prefix}/webapps">
                <include name="${war.file-name}"/>
            </tarfileset>
        </tar>

        <delete file="${packages.dir}/${war.file-name}"/>
        <copy file="${packages.dir}/test-${war.file-name}" tofile="${packages.dir}/${war.file-name}"/>
        <tar destfile="${staging.dir}/test-${app.name}-install.tar" longfile="gnu">
            <tarfileset dir="${packages.dir}" prefix="${install-prefix}/webapps">
                <include name="${war.file-name}"/>
            </tarfileset>
        </tar>

        <delete file="${packages.dir}/${war.file-name}"/>
        <copy file="${packages.dir}/prod-${war.file-name}" tofile="${packages.dir}/${war.file-name}"/>
        <tar destfile="${staging.dir}/prod-${app.name}-install.tar" longfile="gnu">
            <tarfileset dir="${packages.dir}" prefix="${install-prefix}/webapps">
                <include name="${war.file-name}"/>
            </tarfileset>
        </tar>

        <delete file="${packages.dir}/${war.file-name}"/>

        <copy todir="${staging.dir}" file="${tomcat.metadata}/scripts/deploy-${app.name}.sh"/>
    </target>
</project>