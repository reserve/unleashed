
-- Basic data; enterprise --> location
insert into ENTERPRISE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, SHORT_NAME) VALUES
(100, 1, TIMESTAMP '2012-01-01 18:00:00', 'enterprise-100', 'Frontier Vineyards', 'Frontier');

insert into DIVISION (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, ENTERPRISE_ID) VALUES
(100, 1, TIMESTAMP '2012-01-01 18:00:00', 'division-100', 'Frontier Vineyards Division', 100);

insert into PROPERTY (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, DIVISION_ID) VALUES
(100, 1, TIMESTAMP '2012-01-01 18:00:00', 'property-100', 'Frontier Vineyards Property', 100);

insert into SITE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, PROPERTY_ID) VALUES
(100, 1, TIMESTAMP '2012-01-01 18:00:00', 'site-100', 'Frontier Vineyards', 100);
insert into SITE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, PROPERTY_ID) VALUES
(101, 1, TIMESTAMP '2012-01-01 18:00:00', 'site-101', 'Carter Club', 100);

insert into LOCATION (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, SITE_ID) VALUES
(100, 1, TIMESTAMP '2012-01-01 18:00:00', 'location-100', 'Frontier Vineyards Location', 100);
insert into LOCATION (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, SITE_ID) VALUES
(101, 1, TIMESTAMP '2012-01-01 18:00:00', 'location-101', 'Carter Club Location', 101);

-- f764ffdce52b3011ad4f12a6ed93602d == password of 'asmith' salted with 'asmith:<realm>'
insert into APPLICATION_USER (ID, VERSION, LAST_UPDATED, UNIQUE_ID,
       USERNAME, PASSWORD, PASSCODE, FIRST_NAME, LAST_NAME, IS_ACTIVE, ROLE_ID, PRIMARY_ENTERPRISE_ID) VALUES
(101, 1, TIMESTAMP '2012-01-01 18:00:00', 'user-101',
 'asmith', 'f764ffdce52b3011ad4f12a6ed93602d', '1234', 'Anne', 'Smith', 'Y', 1001, 100);