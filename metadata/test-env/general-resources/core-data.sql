-- Use PostgreSQL syntax
set DATABASE SQL SYNTAX PGS TRUE;

-- Need to drop sequences because Hibernate will create them from annotations.
-- Follows the naming convention of PostgreSQL BIGSERIAL data type.
drop sequence ACCOUNT_ID_SEQ;
drop sequence APPLICATION_RIGHT_ID_SEQ;
drop sequence APPLICATION_ROLE_ID_SEQ;
drop sequence APPLICATION_USER_ID_SEQ;
drop sequence BINARY_DATA_ID_SEQ;
drop sequence BINARY_METADATA_ID_SEQ;
drop sequence CONTACT_ID_SEQ;
drop sequence DIVISION_ID_SEQ;
drop sequence ENTERPRISE_ID_SEQ;
drop sequence LOCATION_ID_SEQ;
drop sequence PROPERTY_ID_SEQ;
drop sequence SITE_ID_SEQ;
create sequence ACCOUNT_ID_SEQ as BIGINT start with 50000 increment by 1;
create sequence APPLICATION_RIGHT_ID_SEQ as BIGINT start with 50000 increment by 1;
create sequence APPLICATION_ROLE_ID_SEQ as BIGINT start with 50000 increment by 1;
create sequence APPLICATION_USER_ID_SEQ as BIGINT start with 50000 increment by 1;
create sequence BINARY_DATA_ID_SEQ as BIGINT start with 50000 increment by 1;
create sequence BINARY_METADATA_ID_SEQ as BIGINT start with 50000 increment by 1;
create sequence CONTACT_ID_SEQ as BIGINT start with 50000 increment by 1;
create sequence DIVISION_ID_SEQ as BIGINT start with 50000 increment by 1;
create sequence ENTERPRISE_ID_SEQ as BIGINT start with 50000 increment by 1;
create sequence LOCATION_ID_SEQ as BIGINT start with 50000 increment by 1;
create sequence PROPERTY_ID_SEQ as BIGINT start with 50000 increment by 1;
create sequence SITE_ID_SEQ as BIGINT start with 50000 increment by 1;

-- Roles and rights.
insert into APPLICATION_ROLE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME ) VALUES
(1, 1, TIMESTAMP '2012-01-01 18:00:00', 'role-1', 'System Admin User');
insert into APPLICATION_ROLE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME ) VALUES
(1000, 1, TIMESTAMP '2012-01-01 18:00:00', 'role-1000', 'Application User');
insert into APPLICATION_ROLE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME ) VALUES
(1001, 1, TIMESTAMP '2012-01-01 18:00:00', 'role-1001', 'Application Admin User');

insert into APPLICATION_RIGHT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, RIGHT_TYPE ) VALUES
(1, 1, TIMESTAMP '2012-01-01 18:00:00', 'right-1', 'SYS_ADMIN');
insert into APPLICATION_RIGHT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, RIGHT_TYPE ) VALUES
(1000, 1, TIMESTAMP '2012-01-01 18:00:00', 'right-1000', 'APP_USER');
insert into APPLICATION_RIGHT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, RIGHT_TYPE ) VALUES
(1001, 1, TIMESTAMP '2012-01-01 18:00:00', 'right-1001', 'APP_ADMIN');

insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1, 1);
insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1, 1000);
insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1, 1001);
insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1000, 1000);
insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1001, 1000);
insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1001, 1001);

-- System enterprise instance.
insert into ENTERPRISE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, SHORT_NAME) VALUES
(1, 1, TIMESTAMP '2012-01-01 18:00:00', 'enterprise-1', 'System Enterprise', '-SystemEnterprise-');

-- System user for use in background processing.
insert into APPLICATION_USER (ID, VERSION, LAST_UPDATED, UNIQUE_ID,
       USERNAME, PASSWORD, PASSCODE, FIRST_NAME, LAST_NAME, IS_ACTIVE, ROLE_ID, PRIMARY_ENTERPRISE_ID) VALUES
(1, 1, TIMESTAMP '2012-01-01 18:00:00', 'user-1',
 'system', 'WILLNEVERLOGIN', '2112', 'System', 'User', 'Y', 1, 1);

-- See spring-context-security.xml for the value of <realm>.
-- 17cb9f81d10b5918e2f53ded8eb3ed37 == password of 'rgreinke' salted with 'rgreinke:<realm>'
insert into APPLICATION_USER (ID, VERSION, LAST_UPDATED, UNIQUE_ID,
       USERNAME, PASSWORD, PASSCODE, FIRST_NAME, LAST_NAME, IS_ACTIVE, ROLE_ID, PRIMARY_ENTERPRISE_ID) VALUES
(2, 1, TIMESTAMP '2012-01-01 18:00:00', 'user-2',
 'rgreinke', '17cb9f81d10b5918e2f53ded8eb3ed37', '1234', 'Ross', 'Greinke', 'Y', 1, 1);
